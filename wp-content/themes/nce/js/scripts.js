jQuery(document).ready(function($){
    $(document.body).removeClass('no-js').addClass('js');

    //initPolyfills();

    // Utilities
    initClassToggle();
    initAnchorScroll();
    initProductInfo();
    initBrandInfo();
   
    //initProductSlider();
   // initLandingSlider();
    // CF7 Form Control
    //initCF7();
    //initFullheightMobile();


});

function isMobile() {
    return window.matchMedia('(max-width:767px)').matches;
}



function initPolyfills() {
    // CSS object-fit for IE
    objectFitImages();

    // polyfill for IE - startsWith
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        };
    }

    // polyfill for IE - forEach
    if ('NodeList' in window && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = function (callback, thisArg) {
          thisArg = thisArg || window;
          for (var i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
          }
        };
    }
}

function initLandingSlider() {
  $('.landing__wrap').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 8000,
    prevArrow: false,
    nextArrow: false
  });
}
function initProductSlider() {
  $('.new-arrival__wrap--products').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    prevArrow: false,
    nextArrow: false
  });
}
function initProductInfo() {
  $('.image-slider-wrapper').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    adaptiveHeight: false,
    autoplaySpeed: 2000,
    prevArrow: false,
    nextArrow: false
  });
}
function initBrandInfo() {
  $('.brand-logo').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 6,
    autoplay: true,
    adaptiveHeight: false,
    autoplaySpeed: 2000,
    centerMode: true,
    prevArrow:"<div class='arrow-wrap-left'><a class='slick-prev-one'> <i class='fas fa-chevron-left'></i></a></div>",
    nextArrow:"<div class='arrow-wrap-right'><a class='slick-next-one'> <i class='fas fa-chevron-right'></i></a></div>",
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  });
}


/**
 * Toggle class on click
 */
function initClassToggle() {
    $(document.body).on('click', '[data-toggle="class"][data-class]', function(event) {
        var $trigger = $(this);
        var $target = $($trigger.data('target') ? $trigger.data('target') : $trigger.attr('href'));

        if($target.length) {
            event.preventDefault();
            $target.toggleClass($trigger.data('class'));
            $trigger.toggleClass('classed');
        }
    });
}


/**
 * Smooth anchor scrolling
 */
function initAnchorScroll() {
    $('a[href*="#"]:not([data-toggle])').click(function(event) {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name="'+this.hash.slice(1)+'"]');
            if (target.length && !target.parents('.woocommerce-tabs').length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
            }
        }
    });
}




/*function toggle(b){
$(".hide-one").hide();
(b==1) && $("#generaltac").show();
(b==2) && $("#competitiontac").show();
}/*


/*$('.hamburger').click(function(){
    $(this).toggleClass('open');
    $('ul').slideToggle(400);

});*/

// change menu bg when scroll

/* $(document.body).toggleClass('no-js js');

    // due to menu height including dropdown
    var menuHeight = 70;
    if(window.matchMedia('(max-width:991px)').matches){
        menuHeight = 40;
    }

    // Make menu sticky
    var landingHeight = $('.landing').outerHeight();
    var landingHeight2 = $('.products').outerHeight();

    $(window).scroll(function() {
        var scrollTop = $(window).scrollTop();
        var $siteMenu = $('#site-header');

        if (scrollTop > landingHeight - menuHeight && !$siteMenu.hasClass('site-header__scrolling-active')) {
            $siteMenu.addClass('site-header__scrolling-active');
        } else if (scrollTop < landingHeight - menuHeight && $siteMenu.hasClass('site-header__scrolling-active')) {
            $siteMenu.removeClass('site-header__scrolling-active');
        }
    });*/



// function initCF7(){
//     var wpcf7Elm = $('.wpcf7-form')[0];
//     var formbtn = $('.wpcf7-form input[type="submit"]');

//     formbtn.on('click', function(){
//         formbtn.val('SENDING...');
//     });

//     document.addEventListener( 'wpcf7invalid', function( event ) {
//         formbtn.val('SUBMIT');
//     }, false );

//     document.addEventListener( 'wpcf7mailsent', function( event ) {
//         formbtn.val('SENT!');
//     }, false );
// }

/*function initFullheightMobile() {
    // Fix mobile 100vh change on address bar show/hide
    var lastHeight = $(window).height();
    var heightChangeTimeout = undefined;
    if(isMobile()) {
        $('.vh').css('height', lastHeight);
    }
    (maybe_update_landing_height = function() {
        var winHeight = $(window).height();

        if(heightChangeTimeout !== undefined) {
            clearTimeout(heightChangeTimeout);
        }

        if(!isMobile()) {
            $('.vh').css('height', '');
        }
        else if(Math.abs(winHeight - lastHeight) > 100) {
            heightChangeTimeout = setTimeout(function() {
                var winHeight = $(window).height();
                $('.vh').css('height', winHeight);
                lastHeight = winHeight;
            }, 50);
        }
    })();
    $(window).resize(maybe_update_landing_height);
}*/

/*function initCopyToClipboard() {
  $(document.body).on('click', '[data-copy-to-clipboard]', function(event) {
    event.preventDefault();

    // Source: https://hackernoon.com/copying-text-to-clipboard-with-javascript-df4d4988697f
    var el = document.createElement('textarea');
    el.value = $(this).data('copy-to-clipboard');
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  });
}*/


