<?php
// Template Name: News
the_post();
get_header();

  $newsbannerimage =  get_field('news_background_image');
  $newsimage = $newsbannerimage ['sizes']['large'];
  $newstitle =  get_field('news_background_title');

  $newspagetitle =  get_field('news_title');
  $competitionwinnertitle =  get_field('competition_winner_title');

?>
<section class="news-landing" id="news-products" style="background: url(<?php echo $newsimage ?>) no-repeat center/cover;">
   <div class="body-content">
      <?php if($newstitle):?>
         <h1><?php echo $newstitle ?></h1>
       <?php endif;?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="news" id="news">
  <div class="container">
      <div class="news__wrap">
         <?php if($newspagetitle):?>
            <h1><?php echo $newspagetitle ?></h1>
         <?php endif;?>
         <div class="pt-cv-wrapper">
            <div class="row">
               <div class="col-md-10 col-sm-8">
               <?php echo do_shortcode("[pt_view id=babebf415q show=result]"); ?>
               </div>
               <div class="col-md-2 col-sm-4 d-none d-sm-block">
               <?php echo do_shortcode("[pt_view id=babebf415q show=filter]"); ?>
               </div>
            </div>
         </div>
      </div>
      <!--<div class="news__wrap">
         <?php //if($competitionwinnertitle):?>
            <h1><?php //echo $competitionwinnertitle ?></h1>
         <?php //endif;?>
         <?php //echo do_shortcode("[pt_view id=2864b83t84]"); ?>
      </div>-->
  </div>
</section>

<?php get_footer(); ?>
