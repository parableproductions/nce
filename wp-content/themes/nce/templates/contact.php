<?php
   // Template Name: Contact
  get_header();

  $contactbannerimage =  get_field('contact_background_image');
  $bannercontact = $contactbannerimage ['sizes']['large'];
  $contacttitle =  get_field('contact_background_title');
?>
<section class="contact-landing" id="contact-landing" style="background: url(<?php echo $bannercontact ?>) no-repeat center/cover;">
   <div class="body-content">
      <?php if($contacttitle):?>
         <h1><?php echo $contacttitle ?></h1>
       <?php endif;?>
   </div>
   <div class="overlay-wrap"></div>
</section>
<section class="contact" id="contact">
    <div class="container">
        <div class="contact__wrap">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact__wrap--form">
                        <?php echo do_shortcode('[ninja_form id=3]') ?>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="contact__wrap--map">
                         <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6319.818875028688!2d144.943313!3d-37.627818!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4435a100d59f2558!2sNCE%20Pty%20Ltd!5e0!3m2!1sen!2sau!4v1619153337906!5m2!1sen!2sau" width="100%" height="380" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                   <div class="contact__wrap--info">
                        <h4>Email:<a href="mailto:sales@nce.com.au">sales@nce.com.au</a></h4>
                        <h4>Phone:<a href="tel:1300366024">1300 366 024</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>