<?php
// Template Name: Documents

the_post();
get_header();

$documentbannerimage =  get_field('documents_banner_image');
$bannerdocument = $documentbannerimage ['sizes']['large'];
$documenttitle =  get_field('documents_banner_title');
$docdes =  get_field('documents_banner_description');

$cattitle =  get_field('documents_catalogues_title');
$catimage =  get_field('documents_catalogues_image');
$catdocumentimage = $catimage ['sizes']['medium'];

$softtitle =  get_field('documents_software_title');
$softimage =  get_field('documents_software_image');
$softdocumentimage = $softimage ['sizes']['medium'];

$producttitle =  get_field('documents_product_title');
$productimage =  get_field('documents_product_image');
$productdocumentimage = $productimage ['sizes']['medium'];

?>
<section class="document-landing" id="document-landing" style="background: url(<?php echo $bannerdocument ?>) no-repeat center/cover;">
   <div class="body-content">
      <?php if($documenttitle):?>
      <h1><?php echo $documenttitle ?></h1>
    <?php endif;?>
   </div>
   <div class="overlay-wrap"></div>
</section>


<section class="document" id="document">
   <div class="container">
      <div class="document__intro">
         <?php if($docdes):?>
            <h4><?php echo $docdes ?></h4>
          <?php endif;?>
      </div>
      <div class="document__btn">
            <a href="#catalogues" class="document__btn--wrap" onclick="toggle(1)">
               <div class="wrapper">

                   <?php if($cattitle):?>
                     <h3><?php echo $cattitle ?></h3>
                   <?php endif;?>

                  <img src="<?php lp_image_dir(); ?>/documents/catalogue.png"/>
               </div>
            </a>
            <a href="#softwareupdates" class="document__btn--wrap" onclick="toggle(2)">
               <div class="wrapper">
                   <?php if($softtitle):?>
                     <h3><?php echo $softtitle ?></h3>
                   <?php endif;?>
                  <img src="<?php lp_image_dir(); ?>/documents/softupdate.png"/>
               </div>
            </a>
            <a href="#productmanual" class="document__btn--wrap" onclick="toggle(3)">
               <div class="wrapper">
                  <?php if($producttitle):?>
                     <h3><?php echo $producttitle ?></h3>
                   <?php endif;?>
                  <img src="<?php lp_image_dir(); ?>/documents/productmanual.png"/>
               </div>
            </a>
      </div>

      <div class="document__body">
           <!-- Catalogues documents end-->
         <div class="document__body--wrap items" id="catalogues">
            <div class="title">
               <?php if($cattitle):?>
                  <h2><?php echo $cattitle ?></h2>
                <?php endif;?>
            </div>

            <div class="content-warpper">
               <div class="content-body">
                  <div class="row">
                     <div class="col-md-5"><h4>Description</h4></div>
                     <div class="col-md-5"><h4>Product Code</h4></div>
                     <div class="col-md-2"><h4>Download Link</h4></div>
                  </div>
               </div>

               <!--- this is to be a reapeter -->


                <?php if( have_rows('catalogues_document_settings') ):
                 while( have_rows('catalogues_document_settings') ): the_row();
                     $doclink = get_sub_field('download_link');
                     $documentlink = $doclink['url'] ?? "";

                  ?>
                  <div class="link-info">
                        <div class="row">
                           <div class="col-md-5"><span class="show-mobile">Description:</span><h5><?php echo get_sub_field('document_desecription'); ?></h5></div>
                           <div class="col-md-5"><span class="show-mobile">Product Code:</span><h5><?php echo get_sub_field('product_code'); ?></h5></div>
                           <div class="col-md-2"><span class="show-mobile">Download Link:</span><h5><a href="<?php echo $documentlink ?>" download><?php echo $documentlink ? 'Cick Here' : 'N/A'; ?></a></h5></div>
                        </div>
                     </div>

               <?php endwhile; endif;?>

            </div>
         </div>
         <!-- Catalogues documents end-->
         <!-- Software Updates documents -->
         <div class="document__body--wrap items" id="softwareupdates">
            <div class="title">
               <?php if($softtitle):?>
                  <h2><?php echo $softtitle ?></h2>
                <?php endif;?>
            </div>
             <div class="content-warpper">
                  <div class="content-body">
                  <div class="row">
                     <div class="col-md-5"><h4>Description</h4></div>
                     <div class="col-md-5"><h4>Product Code</h4></div>
                     <div class="col-md-2"><h4>Download Link</h4></div>
                  </div>
               </div>
                <?php if( have_rows('software_document_settings') ):
                 while( have_rows('software_document_settings') ): the_row();
                     $doclink = get_sub_field('download_link');
                     $documentlink = $doclink['url'] ?? "";

                  ?>
                  <div class="link-info">
                        <div class="row">
                           <div class="col-md-5"><span class="show-mobile">Description:</span><h5><?php echo get_sub_field('document_desecription'); ?></h5></div>
                           <div class="col-md-5"><span class="show-mobile">Product Code:</span><h5><?php echo get_sub_field('product_code'); ?></h5></div>
                           <div class="col-md-2"><span class="show-mobile">Download Link:</span><h5><a href="<?php echo $documentlink ?>" download><?php echo $documentlink ? 'Cick Here' : 'N/A'; ?></a></h5></div>
                        </div>
                     </div>

               <?php endwhile; endif;?>
            </div>
         </div>
          <!-- Software Updates documents end-->
           <!-- Products Manual documents -->
         <div class="document__body--wrap items" id="productmanual">
            <div class="title">
               <?php if($producttitle):?>
                  <h2><?php echo $producttitle ?></h2>
                <?php endif;?>
            </div>
           <div class="content-warpper">
                  <div class="content-body">
                  <div class="row">
                     <div class="col-md-5"><h4>Description</h4></div>
                     <div class="col-md-5"><h4>Product Code</h4></div>
                     <div class="col-md-2"><h4>Download Link</h4></div>
                  </div>
               </div>
               <?php if( have_rows('product_document_settings') ):
                 while( have_rows('product_document_settings') ): the_row();
                     $doclink = get_sub_field('download_link');
                     $documentlink = $doclink['url'] ?? "";

                  ?>
                  <div class="link-info">
                        <div class="row">
                           <div class="col-md-5"><span class="show-mobile">Description:</span><h5><?php echo get_sub_field('document_desecription'); ?></h5></div>
                           <div class="col-md-5"><span class="show-mobile">Product Code:</span><h5><?php echo get_sub_field('product_code'); ?></h5></div>
                           <div class="col-md-2"><span class="show-mobile">Download Link:</span><h5><a href="<?php echo $documentlink ?>" download><?php echo $documentlink ? 'Cick Here' : 'N/A'; ?></a></h5></div>
                        </div>
                     </div>

               <?php endwhile; endif;?>
            </div>
         </div>
            <!-- Products Manual documents end-->
      </div>
   </div>
</section>

<script>
  function toggle(a){
      $(".items").hide();
      (a==1) && $("#catalogues").show();
      (a==2) && $("#softwareupdates").show();
      (a==3) && $("#productmanual").show();
   }
</script>


<?php get_footer(); ?>