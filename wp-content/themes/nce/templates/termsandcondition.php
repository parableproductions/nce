<?php
// Template Name: Termsandcondition
the_post();
get_header();

$termsandconditionsbannerimage = get_field('terms_and_conditions_banner_image');
$tacbannerimage = $termsandconditionsbannerimage ['sizes']['large'];
$termsandconditiontitle = get_field('terms_and_conditions_title');
$generaltermsandconditions = get_field('general_terms_and_conditions');
$competitiontermsandconditions = get_field('competition_terms_&_conditions');

?>

<section class="termsandprivacy-landing" id="termsandcondition-landing" style="background: url(<?php echo $tacbannerimage ?>) no-repeat center/cover;">
   <div class="body-content">
       <?php if($termsandconditiontitle):?>
         <h1><?php echo $termsandconditiontitle ?></h1>
       <?php endif;?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="termsandprivacy" id="termsandcondition">
    <div class="container">
          <ul class="tab-navigation">
              <li>General T & C's</li>
              <li>Competition T & C's</li>
          </ul>
          <div class="tab-container-area" id="tab_container_area">
            <div class="tab-container-area__container">
                <?php if ($generaltermsandconditions):?>
                  <?php echo $generaltermsandconditions ?>
                <?php endif; ?>
            </div>
          </div>
          <div class="tab-container-area" id="tab_container_area">
            <div class="tab-container-area__container">
               <?php if ($competitiontermsandconditions):?>
                <?php echo $competitiontermsandconditions ?>
              <?php endif; ?>
            </div>
          </div>
    </div>
</section>



<?php get_footer(); ?>