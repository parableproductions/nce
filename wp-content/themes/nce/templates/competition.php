 <?php
// Template Name: Competition
the_post();
get_header();

$bannerimage =  get_field('banner_background_image');
$banner = $bannerimage ['sizes']['large'];
$bannertitle =  get_field('banner_title');
$comptitle =  get_field('competition_title');
$compdes =  get_field('competition_description');
$compimage =  get_field('competition_image');
$picture = $compimage ['sizes']['large'];
$alt = $compimage['alt'];
$titlecomimg = $compimage['title'];

?>

<section class="competition-banner" id="competition-banner" style="background: url(<?php echo $banner ?>) no-repeat center/cover;">
   <div class="body-content">
    <?php if($bannertitle):?>
      <h1><?php echo $bannertitle ?></h1>
    <?php endif;?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="competition" id="competition">
    <div class="container">
        <div class="competition__body">
            <div class="competition__body--title">
                <?php if($comptitle):?>
                  <h1><?php echo $comptitle ?></h1>
                <?php endif;?>
            </div>
            <div class="competition__body--des">
                <?php if($compdes):?>
                  <p><?php echo $compdes ?></p>
                <?php endif;?>
            </div>
            <div class="competition__body--image">
               <?php if($picture):?>
                  <img src="<?php echo $picture ?>" class="img-fluid" width="100%" alt="<?php echo $alt ?>" title="<?php echo $titlecomimg ?>"/>
                <?php endif; ?>
            </div>
            <div class="competition__body--enquiry">
                <?php echo do_shortcode ('[ninja_form id=4]') ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>

