<?php
// Template Name: About
the_post();
get_header();

$aboutbannerimage =  get_field('about_background_image');
$bannerabout = $aboutbannerimage ['sizes']['large'];
$abouttitle =  get_field('about_background_title');

$aboutbodytitle =  get_field('about_title');
$aboutbodydes =  get_field('about_description');
$aboutbodyimage =  get_field('about_image');
$aboutimage = $aboutbodyimage ['sizes']['large'];

$companybodytitle =  get_field('the_company_title');
$companybodydes =  get_field('the_company_description');
$companybodyimage =  get_field('the_company_image');
$companyimage = $companybodyimage ['sizes']['large'];

$companylearnmorebutton =  get_field('company_learn_more_button');
$companylearnmorebuttonlink =  get_field('company_learn_more_button_link');

$peopletitle =  get_field('our_people_title');
$peopledes =  get_field('our_people_description');

$valuetitle =  get_field('our_value_title');
$ourcommitmentstitle =  get_field('our_commitments_title');
?>
<section class="about-landing" id="about-landing" style="background: url(<?php echo $bannerabout ?>) no-repeat center/cover;">
   <div class="body-content">
       <?php if($abouttitle):?>
         <h1><?php echo $abouttitle ?></h1>
       <?php endif;?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="about" id="about">
   <div class="container">
      <div class="about-wrap">
         <div class="row">
            <div class="col-md-6">
               <div class="title-content">
                  <?php if($aboutbodytitle):?>
                     <h1><?php echo $aboutbodytitle ?></h1>
                   <?php endif;?>

                   <?php if($aboutbodydes):?>
                     <p><?php echo $aboutbodydes ?></p>
                   <?php endif;?>
               </div>

            </div>
            <div class="col-md-6">
               <div class="image-wrap">
                 <!-- <img src="<?php lp_image_dir(); ?>/about_us.jpg"/>-->
                  <img src="<?php echo $aboutimage ?>" class="img-fluid" width="100%"/>
               </div>
            </div>
         </div>
      </div>

      <div class="about-wrap">
         <div class="row">
            <div class="col-md-6 order-md-12">
               <div class="title-content">
                 <?php if($companybodytitle):?>
                     <h1><?php echo $companybodytitle ?></h1>
                   <?php endif;?>

                   <?php if($companybodydes):?>
                     <p><?php echo $companybodydes ?></p>
                   <?php endif;?>
                   <div class="about_btn">
                      <a class="about_btn__ab" href="<?php echo $companylearnmorebuttonlink ?>" target="_blank"><?php echo $companylearnmorebutton ?></a>
                  </div>
               </div>
            </div>
            <div class="col-md-6 order-md-1">
               <div class="image-wrap">
                  <img src="<?php echo $companyimage ?>" class="img-fluid" width="100%"/>
               </div>
            </div>
         </div>
      </div>
   </div>

  </section>


<section class="our-people" id="our-people">
      <div class="container">
         <div class="title-content">
               <?php if($peopletitle ):?>
                  <h1><?php echo $peopletitle ?></h1>
                <?php endif;?>

                <?php if($peopledes):?>
                  <p><?php echo $peopledes ?></p>
                <?php endif;?>
         </div>
         <div class="row">
            <?php if( have_rows('staff_info') ):
                 while( have_rows('staff_info') ): the_row();

                  $staffimage = get_sub_field('staff_image');
                  $stafflargeimage = $staffimage['sizes']['large'];

                  ?>
                  <div class="col-md-6 col-lg-4 mx-auto">
                     <div class="image-wrap">
                           <img src="<?php echo $stafflargeimage ?>"/>
                           <h3><?php echo get_sub_field('staff_name'); ?></h3>
                           <h4><?php echo get_sub_field('staff_title'); ?></h4>
                     </div>
                  </div>
            <?php endwhile; endif;?>
         </div>
      </div>
</section>


<section class="our-value" id="our-value">
   <div class="title-content">
         <?php if($valuetitle):?>
            <h1><?php echo $valuetitle ?></h1>
         <?php endif;?>
   </div>
   <div class="our-value__wrap">
      <?php if( have_rows('our_value_description') ):
           while( have_rows('our_value_description') ): the_row();

            ?>
            <div class="our-value__wrap--body">
               <h3><?php echo get_sub_field('value_heading'); ?></h3>
               <p><?php echo get_sub_field('value_body_text'); ?></p>
            </div>
      <?php endwhile; endif;?>
   </div>
</section>

<section class="our-commitments" id="our-commitments">
   <div class="container">
      <div class="title-content">
         <?php if($ourcommitmentstitle):?>
            <h1><?php echo $ourcommitmentstitle ?></h1>
         <?php endif;?>
      </div>
      <div class="our-commitments__wrap">
         <div class="our-commitments__wrap--left">
            <ul>
                  <?php if( have_rows('our_commitments_list_left') ):
                       while( have_rows('our_commitments_list_left') ): the_row();

                        ?>
                        <li><i class="fas fa-star"></i><h4><?php echo get_sub_field('left_commitments_list'); ?></h4></li>
                  <?php endwhile; endif;?>
            </ul>
         </div>
         <div class="our-commitments__wrap--right">
            <ul>
               <?php if( have_rows('our_commitments_list_right') ):
                       while( have_rows('our_commitments_list_right') ): the_row();

                        ?>
                        <li><i class="fas fa-star"></i><h4><?php echo get_sub_field('right_commitments_list'); ?></h4></li>
                  <?php endwhile; endif;?>
            </ul>
         </div>
      </div>
   </div>
</section>

<?php get_footer(); ?>