<?php
// Template Name: Policies
the_post();
get_header();

$policiesbannerimage = get_field('policies_background_image');
$bannerpolicies = $policiesbannerimage ['sizes']['large'];
$policiestitle = get_field('policies_title');
$policiesdescription = get_field('policies_description');

?>

<section class="termsandprivacy-landing" id="policies-landing" style="background: url(<?php echo $bannerpolicies ?>) no-repeat center/cover;">
   <div class="body-content">
       <?php if($policiestitle):?>
         <h1><?php echo $policiestitle ?></h1>
       <?php endif;?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="termsandprivacy" id="policies">
    <div class="container">
        <div class="policies__wrap">
            <div class="title-content">
                <?php if($policiesdescription):?>
		            <?php echo $policiesdescription ?>
		        <?php endif;?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>