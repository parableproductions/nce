<?php
// Template Name: Service and Repair
the_post();
get_header();

$servicebannerimage =  get_field('service_background_image');
$bannerservice = $servicebannerimage ['sizes']['large'];
$servicetitle =  get_field('service_background_title');

$installfixsolutionstitle =  get_field('install_fix_solutions_title');
$installfixsolutionsimage =  get_field('install_fix_solutions_image');
$installfiximage = $installfixsolutionsimage ['sizes']['large'];
$installfixsolutionstext =  get_field('install_fix_solutions_text');

$installmytitle =  get_field('install_my_title');
$installmytitleimage =  get_field('install_my_image');
$installmyimage = $installmytitleimage ['sizes']['large'];
$iinstallmytext =  get_field('install_my_text');






?>

<section class="service-landing" id="service-landing" style="background: url(<?php echo $bannerservice ?>) no-repeat center/cover;">
   <div class="body-content">
       <?php if($servicetitle):?>
         <h1><?php echo $servicetitle ?></h1>
       <?php endif;?>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="service" id="service">
   <div class="container">
      <div class="service__wrap">
           <div class="title-content">
            	<?php if($installfixsolutionstitle):?>
		         <h2><?php echo $installfixsolutionstitle ?></h2>
		       <?php endif;?>
		       <img src="<?php echo $installfiximage ?>" width="200"/>
              	<?php if($installfixsolutionstext):?>
		         <p><?php echo $installfixsolutionstext ?></p>
		       <?php endif;?>

			</div>
			<div class="title-content">
				<?php if($installmytitle):?>
		         <h2><?php echo $installmytitle ?></h2>
		       <?php endif;?>
		       <img src="<?php echo $installmyimage ?>" width="200"/>
              	<?php if($installfixsolutionstext):?>
		         <p><?php echo $iinstallmytext ?></p>
		       <?php endif;?>

			</div>
        </div>
    </div>
</section>
<?php get_footer(); ?>