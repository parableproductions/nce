<?php
// Template Name: Homepage
   the_post();
   get_header();
   $topvideourl =  get_field('top_video_url');
   $newtitle =  get_field('new_arrival_title');
   $newpara =  get_field('new_arriaval_paragraph');
   $productcatogorytitle =  get_field('product_category_title');
   $productcatogorydes =  get_field('product_category_description');
   $viewallbutton =  get_field('view_all_button');
   $viewallbuttonurl =  get_field('view_all_url');
   $viewallbuttonurllink = $viewallbuttonurl ['url'];
   $brand_title =  get_field('brand_title');
   $brand_para =  get_field('brand_para');

   ?>
<!--<section class="landing" id="landing" style="background: url(<?php //lp_image_dir(); ?>/image01.jpg) no-repeat center/cover;">
    <video id="videoBG" autoplay muted loop>
            <source src="<?php //lp_image_dir(); ?>/intro-video.mp4" type="video/mp4">
            </source>
            <source src="https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_1920_18MG.mp4" type="video/mp4"></source>
        </video>
   <div class="body-content">
      <h1>Your RV Specialist</h1>
      <h3>Proudly serving the RV Industry since 1966</h3>
   </div>
   <div class="overlay-wrap"></div>
</section>-->
<section class="landing" id="landing">
    <video id="videoBG" class="video-bg" autoplay muted loop>
            <source src="<?php echo $topvideourl ?>" type="video/mp4">
            </source>
            <!--<source src="https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_1920_18MG.mp4" type="video/mp4"></source>-->
        </video>
   <div class="body-content">
      <h1>Your RV Specialist</h1>
      <h3>Proudly serving the RV Industry since 1966</h3>
   </div>
   <div class="arrow-down"> <a href="#new-arrival"><i class="fas fa-chevron-down"></i></a></div>
   <div class="overlay-wrap"></div>
</section>



<section class="new-arrival" id="new-arrival">
   <div class="container">
   <div class="new-arrival__wrap">
      <div class="title">

         <?php if($newtitle):?>
            <h1><?php echo $newtitle ?></h1>
         <?php endif;?>
         <?php if($newpara):?>
            <p><?php echo $newpara ?></p>
         <?php endif;?>
      </div>
      <div class="new-arrival__wrap--products">
        <!--<?php //echo do_shortcode("[pt_view id=46f1a83mcw]"); ?>-->
        <?php echo do_shortcode('[woo_product_slider id="1173"]'); ?>
      </div>

   </div>
</section>

<section class="image-slider" id="image-slider">
	<div class="image-slider-wrapper" id="image-slider">
		<?php if( have_rows('slider') ):
	     while( have_rows('slider') ): the_row();

	     	$imageslider = get_sub_field('image');
	   		$largesliderimage = $imageslider['sizes']['large'];
	   		$productlink =  get_sub_field('product_link');

	   		?>
            <a href="<?php echo $productlink ?>">
	   		    <div class="image-slider-wrapper__wrap" style="background: url(<?php echo $largesliderimage ?>) no-repeat center/cover;">
			      <div class="overlay-wrap"></div>
			      <div class="overlay-text">
			         <h2><?php echo get_sub_field('title_text'); ?></h2>
							<p><?php echo get_sub_field('description'); ?></p>
			      </div>
			   </div>
			  </a>
	    <?php endwhile; endif;?>
	</div>
</section>

<section class="new-arrival" id="new-arrival">
   <div class="container">
   <div class="new-arrival__wrap">
      <div class="title">
      	<?php if($productcatogorytitle):?>
            <h1><?php echo $productcatogorytitle ?></h1>
         <?php endif;?>
         <?php if($productcatogorydes):?>
            <p><?php echo $productcatogorydes ?></p>
         <?php endif;?>

      </div>
      <div class="new-arrival__wrap--products">
         <div class="row">
         	 <?php if( have_rows('product_category_info') ):
				     while( have_rows('product_category_info') ): the_row();

				     	$image = get_sub_field('image');
				   		$mediumimage = $image['sizes']['medium'];
				   		$producturl =  get_sub_field('product_url');
                      $producturllink = $producturl ['url'];

				     	?>
            	<div class="col-sm-12 col-md-6 col-lg-4 mx-auto">
	                <a href="<?php echo $producturllink ?>">
		                  <img src="<?php echo $mediumimage; ?>">
		                  <div class="products-details">
		                     <h4><?php echo get_sub_field('category_title'); ?></h4>
		                  </div>
		    		</a>
            	</div>
            <?php endwhile; endif;?>
         </div>
         <div class="view-more-btn-wrap">
         	  <a href="<?php echo $viewallbuttonurllink ?>" class="view-more-btn"><?php echo $viewallbutton ?></a>
         </div>
      </div>
   </div>
</section>
<section class="brand" id="brand">
   <div class="container">
      <div class="title">
      	<?php if($brand_title):?>
            <h1><?php echo $brand_title ?></h1>
         <?php endif;?>
         <?php if($brand_para):?>
            <p><?php echo $brand_para ?></p>
         <?php endif;?>
      </div>
      	<div class="brand-logo">
	 		<?php if( have_rows('brand_image') ):
			     while( have_rows('brand_image') ): the_row();
		     	$imagelogo = get_sub_field('logo_image');
		   		$smallimagelogo = $imagelogo['sizes']['medium'];

		     	?>

		     	 <div class="grid-item">
		         	<div class="logo">
		         		<img src="<?php echo $smallimagelogo; ?>" width="150">
		         	</div>
		         </div>
        	<?php endwhile; endif;?>
      </div>
   </div>
</section>
<section class="subscription" id="subscription">
   <div class="container">
      <div class="title">
         <h1>Contact Us</h1>
      <div class="subscription-form">
          <?php echo do_shortcode ('[ninja_form id=2] ') ?>
      </div>
   </div>
</section>
<?php get_footer(); ?>



<?php get_footer(); ?>