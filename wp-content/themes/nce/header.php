<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!--<link rel="profile" href="https://kenwheeler.github.io/slick/slick/slick.js">-->
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

	<script type="text/javascript">
		var _ajaxurl = '<?= admin_url("admin-ajax.php"); ?>';
		var _pageid = '<?= get_the_ID(); ?>';
		var _imagedir = '<?php lp_image_dir(); ?>';
	</script>
	 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">


</head>
<body <?php body_class(); ?>>

<?php

// This fixes an issue where wp_nav_menu applied the_title filter which causes WC and plugins to change nav menu labels
print '<!--';
the_title();
print '-->';

$footer_logo = get_field('footer_logo','option');
$footerlogo = $footer_logo['sizes']['large'];

?>


<div id="top"></div>
<div class="wrapper">

	<div class="sticky-btn">
		<div class="sticky-btn__wrap">
			<a target="_blank" href="https://www.homeandrv.com.au/"><img src="<?php lp_image_dir(); ?>/h&rv.png"/></a><br/>
			<a target="_blank" href="https://www.facebook.com/NCERVSpecialist/"><i class="fab fa-facebook-square"></i></a><br/>
			<a target="_blank" href="https://www.youtube.com/channel/UC11YvG4LlcxNmrS7Zwz8L5w"><i class="fab fa-youtube-square"></i></a>
		</div>
	</div>
	<header class="site-header" id="site-header">
		<!-- Header goes here -->

		<div class="custom-container">
			<div class="menu-bar">
				<div class="menu-bar__left">
					<a href="/"><img class="logo" src="<?php echo $footerlogo; ?>" width="75" style="height:40px;"></a>
				</div>
				<div class="menu-bar__right">
					<div class="mobile-logo">
						<a href="/"><img class="logo" src="<?php echo $footerlogo; ?>" width="75" style="height:40px;"></a>
					</div>
					<?php ubermenu( 'main' , array( 'theme_location' => 'header-menu' ) ); ?>
				</div>
			</div>
		</div>

		<!--<div class="container">
			<div class="menu-bar">
				<div class="menu-bar__left">
					<a href="/"><img class="logo" src="<?php lp_image_dir(); ?>/logo.png"/ width="75"></a>
				</div>
				<div class="menu-bar__right">
					<?php //ubermenu( 'main' , array( 'theme_location' => 'header-menu' ) ); ?>
				</div>
			</div>
		</div>-->
	</header>