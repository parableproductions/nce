<?php the_post(); get_header();
$newsbannerimage =  get_field('news_background_image',65);
$newsimage = $newsbannerimage ['sizes']['large'];
?>

<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
<section class="news-landing" id="news-products" style="background: url('<?php echo $newsimage ?>') no-repeat center/cover;">
   <div class="body-content">
      <!--<h1><?php the_title(); ?></h1>-->
      <h1>News and Updates</h1>
   </div>
   <div class="overlay-wrap"></div>
</section>

<div class="container">

	<div class="single_wrap">
		<div class="single_wrap__title">
			<?php the_title(); ?>
		</div>
		<?php $thumb = get_the_post_thumbnail_url(); ?>
		<div class="single_wrap__image" style="background: url('<?php echo $thumb;?>') no-repeat center/cover;">
			<!--<?php the_post_thumbnail(); ?>-->
		</div>
		<div class="single_wrap__content">
			<?php the_content(); ?>
		</div>

		<div class="single_wrap__content mt-5">
			<a class="view-more-btn-sp" href="/nce/news">Back to News</a>
		</div>
	</div>
</div>

<?php get_footer(); ?>