<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
defined( 'ABSPATH' ) || exit;
get_header( 'shop' );

?>

<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );?>

<section class="product-landing" id="product-landing" style="background: url('<?php echo $thumb['0']; ?>') no-repeat center/cover;">
   <div class="body-content">
      <h1>Our Products</h1>
   </div>
   <div class="overlay-wrap"></div>
</section>

<section class="stocklist" id="stocklist">


	<!--<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );?>
    <div class="top-header" id="top-header"
        style="background: url('<?php echo $thumb['0']; ?>') no-repeat center/cover;">-->
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    <!--</div>-->


    <!-- INTRO -->
   <!-- <header class="woocommerce-products-header">
        <div class="stocklist__introduction">

            <div class="container">-->
                <!-- PAGE DESCRIPTION -->
               <!-- <?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	//do_action( 'woocommerce_archive_description' ); ?>-->
                <!-- PAGE DESCRIPTION -->
            <!--</div>
        </div>
    </header>-->
    <!-- /INTRO -->
    
    
    <div class="new-arrival product_newarrivals">
    	<div class="container">
			<div class="new-arrival__info">
				 <h1>New Arrivals</h1>
			</div>
			
			 <?php echo do_shortcode('[woo_product_slider id="1173"]'); ?>


	    </div>
	    <div class="arrow-down"> <a href="#product-wrapper">click here for all product<br/><i class="fas fa-chevron-down"></i></a></div>
	</div>


    <!-- WOOCOMMERCE LISTINGS START -->
    <div class="stocklist__listings">
        <div class="container">
    		<div class="stocklist__listings--wrap">
                <div class="stocklist__listings--stock">

				    <div class="product-wrapper" id="product-wrapper">
						<div>

						<?php echo do_shortcode('[flexy_breadcrumb]');?>
							<!-- <?php 
								/**
								 * Hook: woocommerce_before_main_content.
								 *
								 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
								 * @hooked woocommerce_breadcrumb - 20
								 * @hooked WC_Structured_Data::generate_website_data() - 30
								 */
								//do_action( 'woocommerce_before_main_content' );
							?> -->
						</div>
				    	<div class="product-title mt-5 mb-5">
				    		<h1>Product List</h1>
				    	</div>
				    	<div class="row">
				    		<div class="col-md-12 col-lg-3">
					    	 	<!--<h2>Filter products</h2>-->
					    	 	<div class="filter-wrap">
					    	 		<?php echo do_shortcode("[annasta_filters preset_id=1]"); ?>
					    	 	</div>
					    	</div>
					    	<div class="col-md-12 col-lg-9">



					    		<div class="stocklist__introduction">
					    			 <!-- PAGE DESCRIPTION -->
					               <h2><?php do_action( 'woocommerce_archive_description' ); ?></h2>
					                <!-- PAGE DESCRIPTION -->
								 </div>


		                        <!-- START LISTINGS -->
		                        <?php if ( woocommerce_product_loop() ) {
								do_action( 'woocommerce_before_shop_loop' );
								woocommerce_product_loop_start(); ?>
		                        <?php
								if ( wc_get_loop_prop( 'total' ) ) {
									while ( have_posts() ) {
										the_post();
										do_action( 'woocommerce_shop_loop' );

										wc_get_template_part( 'content', 'product' );
									}
								}
								woocommerce_product_loop_end(); ?>

		                        <?php
								do_action( 'woocommerce_after_shop_loop' );
								?>
		                        <!-- END LISTISNG-->

		                        <?php } else {
									do_action( 'woocommerce_no_products_found' );
								}
									do_action( 'woocommerce_after_main_content' );
								?>
							</div>
						</div>
               		 </div>
        		</div>
    		</div>
    	</div>
    </div>
</section>

<script>
	$('.awf-filter-container input').click(function(){
		 
		if($('.awf-active-badges-container div').length >= 0){
			$('.term-description p').hide();
		}
	})
</script>

<?php get_footer('shop');