<?php

require 'inc/constants.php';

require 'inc/acf.php';
require 'inc/admin.php';
require 'inc/ajax.php';
require 'inc/enqueue.php';
require 'inc/forms.php';
require 'inc/markup.php';
require 'inc/media.php';
require 'inc/menus.php';
require 'inc/misc.php';
require 'inc/query.php';
require 'inc/register.php';


// Set the homepage <title> attribute;
/*function different_document_title($title) {
    if(is_front_page()) {
        $title = get_bloginfo('name');
    }

    return $title;
}
add_filter('pre_get_document_title', 'different_document_title', 100);*/

// Reroute inaccessible pages to 404
function reroute_to_404($template) {
	if(get_field('page_inaccessible')) {
		return locate_template('404.php');
	}
	else {
		return $template;
	}
}
add_filter('template_include', 'reroute_to_404');

// Redirect to a URL
function redirect_to_url() {
	$redirect = get_field('page_redirect');
	if($redirect && $redirect['url']) {
		wp_redirect($redirect['url']);
	}
}
add_action('template_redirect', 'redirect_to_url');

function widgets_init(){
	register_sidebar(array(
	'name' => __('Scrisoft Sidebar' , 'scrisoft'),
	'id' => 'sidebar-1',
	'before_title' => '<div id="cat">',
	'after_title' => '</div>',
	));

    register_sidebar(array(
        'name' => __('News Sidebar' , 'news'),
        'id' => 'sidebar-2',
        'before_title' => '<div id="cat">',
        'after_title' => '</div>',
        ));
}
add_action('widgets_init', 'widgets_init');

function my_custom_woocommerce_theme_support() {
    add_theme_support( 'woocommerce', array(
        // . . .
        // thumbnail_image_width, single_image_width, etc.
        // Product grid theme settings
        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 4,
            'min_columns'     => 2,
            'max_columns'     => 5,
        ),
    ) );
}
add_action( 'after_setup_theme', 'my_custom_woocommerce_theme_support' );


//Image Size for Competition Page
add_image_size('my_custom_size', 900, 300, true);


//Remove Add to Cart Option from the Page to be Displayed.
//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );