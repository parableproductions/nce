	<footer class="site-footer">
		<!-- Footer goes here -->
		<div class="site-footer__wrapper">
            <div class="top-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3">
                            <div class="logo">
                                <?php
                                    $footer_logo = get_field('footer_logo','option');
                                    $footerlogo = $footer_logo['sizes']['large'];
                                ?>
                                <!-- <a href="<?php //echo get_site_url(); ?>" target="_blank"><img src="<?php //lp_image_dir(); ?>/logo.png"></a> -->
                                <a href="<?php echo get_site_url(); ?>" target="_blank"><img src="<?php echo $footerlogo; ?>"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2">
                             <ul>
                                <?php
                                    if( have_rows('footer_menu_one','option') ):
                                        while( have_rows('footer_menu_one','option') ) : the_row();
                                        ?>
                                        <li><a href="<?php echo get_sub_field('link','option')?>"><?php echo get_sub_field('link_title','option');?></a></li>
                                    <?php endwhile;
                                endif;?>



                                 <!--<li><a href="<?php //echo get_site_url(); ?>/news/">News</a></li>
                                <li><a href="<?php //echo get_site_url(); ?>/shop/">Products</a></li>
                                <li><a href="https://www.homeandrv.com.au/" target="_blank">Shop now</a></li>-->
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2">
                             <ul>
                                <?php
                                    if( have_rows('footer_menu_two','option') ):
                                        while( have_rows('footer_menu_two','option') ) : the_row();
                                        ?>
                                        <li><a href="<?php echo get_sub_field('link','option')?>"><?php get_sub_field('link_title','option');?></a></li>
                                    <?php endwhile;
                                endif;?>
                                <!--<li><a href="<?php //echo get_site_url(); ?>/contact/">Contact Us</a></li>
                                <li><a href="<?php //echo get_site_url(); ?>/about/">About Us</a></li>
                               <li><a href="<?php //echo get_site_url(); ?>/competition/">Competition</a></li>
                               <li><a href="<?php //echo get_site_url(); ?>/service-and-repair/">Service & Repair</a></li>-->
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2">
                            <ul>
                                
                               <?php
                                    if( have_rows('footer_menu_three','option') ):
                                        while( have_rows('footer_menu_three','option') ) : the_row();
                                        ?>
                                        <li><a href="<?php echo get_sub_field('link','option')?>"><?php get_sub_field('link_title','option');?></a></li>
                                    <?php endwhile;
                                endif;?>
                                <!--<li><a href="<?php //echo get_site_url(); ?>/policies/">Policies</a></li>
                                <li><a href="<?php //echo get_site_url(); ?>/terms-and-conditions/">Terms & Conditions</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#PolicyModalLong">Policies</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#TermsModalLong">Terms & Conditions</a></li>-->
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3">

                            <ul>
                                <?php
                                    $phonenumber = get_field('phone_number' ,'option');
                                    $emailcontact = get_field('email_contact','option');
                                ?>
                                <?php if($phonenumber):?>
                                    <li><a href="tel:<?php echo $phonenumber ?>"><?php echo $phonenumber ?></a></li>
                                <?php endif;?>

                                 <?php if($emailcontact):?>
                                    <li><a href="mailto:<?php echo $emailcontact ?>"><?php echo $emailcontact ?></a></li>
                                <?php endif;?>
                            </ul>
                            <div class="social-icon">
                                <ul>
                                    <li><a class="facebook" target="_blank" href="https://www.facebook.com/NCERVSpecialist"><i class="fab fa-facebook-square"></i></a></li>
                                    <li><a class="youtube" target="_blank" href="https://www.youtube.com/channel/UC11YvG4LlcxNmrS7Zwz8L5w"><i class="fab fa-youtube-square"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bottom-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <?php
                                $copyrightdata = get_field('copyright' ,'option');
                            ?>
                            <?php if($copyrightdata):?>
                                 <p><?php echo $copyrightdata ?></p>
                            <?php endif;?>
                        </div>
                        <div class="col-sm-12 col-md-4 text-right">
                            <!--<p><a href="#" target="_blank">Policies</a> <a href="#" target="_blank">Terms & Conditions</a></p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</footer>
</div> <!-- end wrapper -->
<?php wp_footer(); ?>


<!-- Modal for Privacy Policy -->
<div class="modal fade" id="PolicyModalLong" tabindex="-1" role="dialog" aria-labelledby="PolicyModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
            <div class="modal-content">
                  <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">POLICIES</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                  </div>
                  <div class="modal-body">
                        <div class="modal-body__wrap">
                                    <h5 class="font_5" style="font-size:31px">PRIVACY POLICY</h5>
                                    <p class="font_8" style="font-size:18px">This following document sets forth the Privacy Policy for the NCE Pty Ltd website,<a data-auto-recognition="true" href="http://www.nce.com.au" target="_blank">www.nce.com.au</a>.</p>

                                    <p class="font_8" style="font-size:18px">NCE Pty Ltd is committed to providing you with the best possible customer service experience. NCE Pty Ltd is bound by the Privacy Act 1988 (Crh), which sets out a number of principles concerning the privacy of individuals.</p>

                                    <p class="font_7" style="font-size:22px">Collection of your personal information</p>

                                    <p class="font_8" style="font-size:18px">There are many aspects of the site which can be viewed without providing personal information, however, for access to future NCE Pty Ltd customer support features you are required to submit personally identifiable information. This may include but not limited to a unique username and password, or provide sensitive information in the recovery of your lost password.</p>

                                    <p class="font_8" style="font-size:18px">NCE Pty Ltd respects your privacy. We will only use the information you provide for the purposes of responding to your request or enquiry. &nbsp;We may disclose your information to our related companies and/or third parties associated with us for these purposes. Our Privacy Policy, available here, contains further details regarding how you can access or correct information we hold about you, how you can make a privacy related complaint, how that complaint will be dealt with and the extent to which your information may be disclosed to overseas recipients.</p>

                                    <p class="font_7" style="font-size:22px">Sharing of your personal information</p>

                                    <p class="font_8" style="font-size:18px">We may occasionally hire other companies to provide services on our behalf, including but not limited to handling customer support enquiries, processing transactions or customer freight shipping. Those companies will be permitted to obtain only the personal information they need to deliver the service. NCE Pty Ltd takes reasonable steps to ensure that these organisations are bound by confidentiality and privacy obligations in relation to the protection of your personal information.</p>

                                    <p class="font_7" style="font-size:22px">Use of your personal information</p>

                                    <p class="font_8" style="font-size:18px">For each visitor to reach the site, we expressively collect the following non-personally identifiable information, including but not limited to browser type, version and language, operating system, pages viewed while browsing the Site, page access times and referring website address. This collected information is used solely internally for the purpose of gauging visitor traffic, trends and delivering personalized content to you while you are at this Site.</p>

                                    <p class="font_8" style="font-size:18px">From time to time, we may use customer information for new, unanticipated uses not previously disclosed in our privacy notice. If our information practices change at some time in the future we will use for these new purposes only, data collected from the time of the policy change forward will adhere to our updated practices.</p>


                                    <p class="font_7" style="font-size:22px">Changes to this Privacy Policy</p>

                                    <p class="font_8" style="font-size:18px">NCE Pty Ltd reserves the right to make amendments to this Privacy Policy at any time. If you have objections to the Privacy Policy, you should not access or use the Site.</p>

                                    <p class="font_7" style="font-size:22px">Accessing Your Personal Information</p>

                                    <p class="font_8" style="font-size:18px">You have a right to access your personal information, subject to exceptions allowed by law. If you would like to do so, please let us know. You may be required to put your request in writing for security reasons. NCE Pty Ltd reserves the right to charge a fee for searching for, and providing access to, your information on a per request basis.</p>

                                    <h5 class="font_5" style="font-size:31px">REPAIR, RETURN &amp; REFUND POLICY</h5>

                                    <p class="font_7" style="font-size:22px">About our policy</p>

                                    <p class="font_8" style="font-size:18px">At NCE Pty Ltd (NCE) we want our customers to be completely satisfied with their purchase. We therefore recommend that you read and become familiar with our Repair, Return and Refund Policy and our NCE General Terms and Conditions of Trade.&nbsp;</p>

                                    <p class="font_8" style="font-size:18px"><br />
                                    Our goods come with guarantees that cannot be excluded under the Australian Consumer Law. <span style="font-style:italic;">You are entitled to a replacement or refund for a major failure and for compensation for any other reasonably foreseeable loss or damage. &nbsp;You are also entitled to have the goods repaired or replaced if the goods fail to be of acceptable quality and the failure does not amount to a major failure.</span></p>

                                    <p class="font_8" style="font-size:18px"><br />
                                    This document can be found&nbsp;<span style="text-decoration:underline"><a href="https://ariticapp.nce.com.au/ma/asset/84:repair-return--refund-policy" target="_blank" rel="noopener">here</a></span>.</p>

                                    <p class="font_7" style="font-size:22px">Warranty Conditions</p>

                                    <p class="font_8" style="font-size:18px">NCE offers the following warranty in relation to its goods.&nbsp;</p>

                                    <p class="font_8" style="font-size:18px">
                                    The benefits of this warranty are in addition to any rights and remedies imposed by Australian State and Federal legislation that cannot be excluded. &nbsp;Nothing in this warranty is to be interpreted as excluding, restricting or modifying any State or Federal legislation applicable to the supply of goods and services which cannot be excluded, restricted or modified.</p>

                                    <p class="font_8" style="font-size:18px">
                                    NCE warrants to the original purchaser that, subject to the exclusions and limitations below, all parts of the manufacture and assembly of the goods carried out by NCE will be free from defects in materials and workmanship for a period of 12 months from the date of purchase <span style="font-weight:bold;">(Warranty Period).</span></p>

                                    <p class="font_8" style="font-size:18px">
                                    This warranty is not transferable to a subsequent customer if the goods are sold by the original customer during the Warranty Period.</p>

                                    <p class="font_8" style="font-size:18px">
                                    If a defect appears in NCE&#39;s manufacture or assembly of the goods before the end of the Warranty Period and NCE finds the goods to be defective in materials or workmanship, NCE will, in its sole discretion, either repair or replace the goods or the defective part of the goods free of charge, or provide a credit or exchange.</p>

                                    <p class="font_8" style="font-size:18px">
                                    NCE reserves the right to replace defective parts of the goods with parts and components of similar quality, grade and composition where an identical part or component is not available. &nbsp;Goods presented for repair may be replaced by refurbished goods of the same type rather than being repaired. Refurbished parts may be used to repair the goods.<br />
                                    Where your rights under the Australian Consumer Law or this warranty do not apply, we may provide you with an indicative cost estimate to repair the goods.</p>

                                    <p class="font_7" style="font-size:22px">Change of mind</p>

                                    <p class="font_8" style="font-size:18px">Please choose carefully as credits and returns are not provided where you have simply changed your mind or made a wrong selection. We recommend that you carefully review any orders before proceeding. Goods can not be accepted for return unless agreed in writing by NCE and a restocking charge of 25% may apply.</p>

                                    <p class="font_7" style="font-size:22px">Damaged goods</p>

                                    <p class="font_8" style="font-size:18px">NCE recommends that you immediately inspect any goods that we deliver or that you collect from any of our warehouses to ensure you are completely satisfied with the goods, including that the goods are of acceptable quality, and match the description we have provided to you.</p>

                                    <p class="font_8" style="font-size:18px">
                                    If any goods arrive damaged, please contact your NCE Account Manager as soon as possible so a Return Authority Number can be arranged for the goods to be inspected. Goods must be returned within a reasonable time. The acceptance of the goods delivered shall be deemed for all purposes to have taken place 30 days* from the date of delivery.<br />
                                    If a purchaser receives a product that is damaged in transit from NCE, the purchaser should:<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Refuse to accept delivery of the product;<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Direct the courier to &quot;Return goods to sender&quot;; and<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Notify the NCE Customer Service or Warranty Department immediately.</p>

                                    <p class="font_8" style="font-size:18px">
                                    NCE will not accept warranty claims on items delivered to the requested destination and inspected at time of delivery by the purchaser that are subsequently deemed to be damage in transit, after 7 days of delivery from the delivery date.</p>

                                    <p class="font_8" style="font-size:18px"><br />
                                    Your Account Manager may take the following steps in order to determine the best way to proceed:<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Visit you on site to inspect the goods within 5 working days* from your initial contact with NCE; or<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Where your location prevents an immediate on-site visit, we may ask you to email photos documenting the damage to your Account Manager.<br />
                                    Your Account Manager will require the following information to ensure your claim is resolved as quickly as possible:<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Original invoice number<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Description of damage or repair required<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Photos of damage or repair required<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Chassis number<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Make, model or part number<br />
                                    &bull;&nbsp;&nbsp; &nbsp;Caravan build date&nbsp;</p>

                                    <p class="font_8" style="font-size:18px">
                                    No goods will be accepted for return until a Return of Goods Authority Number has been supplied to you. Goods must be returned in the condition received by you with all original packaging, accessories and/ or manuals.</p>

                                    <p class="font_7" style="font-size:22px">Returns and repairs</p>

                                    <p class="font_8" style="font-size:18px">Goods returned for repair or credit will be assessed and repaired or replaced within a reasonable time. Credits will normally be processed within 14 days* of your goods being returned to NCE&rsquo;s nominated warehouse. Where goods have been assessed to be repairable under the manufacturers&rsquo; warranty, you may be supplied with details of an authorised repairer. You may also be provided with an indicative repair and/ or replacement time, which may vary due to reasons beyond our control, or the repairer&rsquo;s reasonable control, such as part availability and incorrect fault description. &nbsp;NCE does not take any responsibility for any repairs and/ or replacements carried out without our prior written consent.&nbsp;</p>

                                    <p class="font_8" style="font-size:18px">Where goods are assessed to have been damaged by misuse or accident, no credit will be issued and no further action will be entered into. &nbsp;Where your rights under the Australian Consumer Law or any manufacturer&rsquo;s warranty do not apply, we may provide you with an indicative cost estimate to repair the goods.&nbsp;</p>

                                    <p class="font_8" style="font-size:18px">NCE will endeavor to locate a service agent within a reasonable and acceptable distance to the caravan&rsquo;s location (within 50kms). If this is not possible, the claimant/owners travel plans and next &lsquo;major&rsquo; town or city location will be required.</p>

                                    <p class="font_8" style="font-size:18px">If a replacement item is required, NCE will require the location of the caravan and owners for the next 14 days to allow for pick, pack and postage. If the owners are traveling, NCE require next major town or city location.&nbsp;Otherwise, the warranty procedure as in place at present will remain.</p>

                                    <p class="font_7" style="font-size:22px">Replacement item warranty</p>

                                    <p class="font_8" style="font-size:18px">Should a replacement item be supplied by NCE &lsquo;under warranty&rsquo; due to damage or product failure of original item purchased, NCE will warrant the replacement item for the remaining warranty period only of the original item warranty.</p>

                                    <p class="font_8" style="font-size:18px">
                                    <span style="text-decoration:underline;">Warranty Claims</span><br />
                                    1.&nbsp;If a fault covered by warranty occurs, the customer must first contact the NCE Customer Service or Warranty Department.</p>

                                    <p class="font_8" style="font-size:18px">
                                    2.&nbsp; Any warranty claim must be accompanied by proof of purchase, full details of the alleged defect (including clear photos), and appropriate documentation (such as historical and maintenance records).</p>

                                    <p class="font_8" style="font-size:18px">
                                    3.&nbsp;The customer must make the goods available to NCE or its authorised repair agent for inspection and testing.</p>

                                    <p class="font_8" style="font-size:18px">
                                    4.&nbsp;If such inspection and testing finds no defect in the goods, the customer must pay NCE&#39;s usual costs of service work and testing. &nbsp;The customer must bear the cost of the transport of the goods to and from NCE or the authorised repair agent, and all insurance of the goods.</p>

                                    <p class="font_8" style="font-size:18px">
                                    5. Goods returned for repair or credit will be assessed and repaired or replaced within a reasonable time.</p>

                                    <p class="font_8" style="font-size:18px">
                                    6.&nbsp; Credits will normally be processed within 14 days* of your goods being returned to NCE&rsquo;s nominated warehouse.</p>

                                    <p class="font_8" style="font-size:18px">
                                    7.&nbsp;Where goods have been assessed to be repairable under this warranty, you may be supplied with details of an authorised repairer. You may also be provided with an indicative repair and/ or replacement time, which may vary due to reasons beyond our control, or the repairer&rsquo;s reasonable control, such as part availability and incorrect fault description.</p>

                                    <p class="font_8" style="font-size:18px">
                                    8.&nbsp;NCE does not take any responsibility for any repairs and/ or replacements carried out without our prior written consent.</p>

                                    <p class="font_8" style="font-size:18px">
                                    9.&nbsp;NCE will endeavour to locate a service agent within a reasonable and acceptable distance to the caravan&rsquo;s location (within 50kms). If this is not possible, the claimant/owners travel plans and next &lsquo;major&rsquo; town or city location will be required.</p>

                                    <p class="font_8" style="font-size:18px">
                                    10.&nbsp;If a replacement item is required, NCE will require the location of the caravan and owners for the next 14 days to allow for pick, pack and postage. If the owners are traveling, NCE require next major town or city location.</p>

                                    <p class="font_7" style="font-size:22px">Warranty repairs on caravans</p>

                                    <p class="font_8" style="font-size:18px">NCE will not accept any claim for reimbursement for repairs or rectification carried out without prior authorisation from NCE Management. A written quote for the repair can be supplied, however NCE reserves the right to compare and assess the quote with an alternative repairer.</p>

                                    <p class="font_8" style="font-size:18px"><span style="font-style:normal"><span style="font-size:22px">Exclusions</span></span></p>

                                    <p class="font_8" style="font-size:18px">Where the warranty does not apply, no credit will be issued and no further action will be entered into.</p>

                                    <p class="font_8" style="font-size:18px">
                                    The warranty will not apply where:<br />
                                    (a)&nbsp;&nbsp; &nbsp;the goods have been repaired, altered or modified by someone other than NCE or an authorised repair agent;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (b)&nbsp;&nbsp; &nbsp;the alleged defect in the goods is within acceptable industry variances;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (c)&nbsp;&nbsp; &nbsp;NCE cannot establish any fault in the goods after testing and inspection;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (d)&nbsp;&nbsp; &nbsp;the goods have been used other than for the purpose for which it was designed;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (e)&nbsp;&nbsp; &nbsp;the defect in the goods has arisen due to the customer&#39;s failure to properly use and maintain the goods in accordance with NCE&#39;s instructions, recommendations and specifications (including applicable maintenance schedules);</p>

                                    <p class="font_8" style="font-size:18px">
                                    (f)&nbsp;&nbsp; &nbsp;the defect in the goods has arisen due to the customer&#39;s request to customise the goods;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (g)&nbsp;&nbsp; &nbsp;the goods have been subject to abnormal conditions, including environment, temperature, water, fire, humidity, pressure, stress or similar;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (h)&nbsp;&nbsp; &nbsp;the defect has arisen due to abuse, misuse, neglect or accident;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (i)&nbsp;&nbsp; &nbsp;unauthorised parts or accessories have been used on or in relation to the goods; or&nbsp;</p>

                                    <p class="font_8" style="font-size:18px"><
                                    (j)&nbsp;&nbsp; &nbsp;the goods has been overloaded or involved in an accident.</p>

                                    <p class="font_8" style="font-size:18px">
                                    The warranty does not extend to:<br />
                                    (a)&nbsp;&nbsp; &nbsp;damage or defects caused by normal wear and tear, including impact or stone damage;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (b)&nbsp;&nbsp; &nbsp;water damage caused by creek crossings, flooding and other similar conditions;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (c)&nbsp;&nbsp; &nbsp;damage or defects caused by excessive speed, hard impact or use of the goods in unsuitable 4WD or off-road applications;</p>

                                    <p class="font_8" style="font-size:18px">
                                    (d)&nbsp;&nbsp; &nbsp;the aesthetics of galvanising, coating and protectant treatments used.<br />
                                    &nbsp;</p>

                                    <p class="font_8" style="font-size:18px"><span style="font-style:normal"><span style="font-weight:400"><span style="font-size:22px">Limitations</span></span></span></p>

                                    <p class="font_8" style="font-size:18px">NCE makes no express warranties or representations other than set out in this warranty.<br />
                                    The repair or replacement of the goods or part of the goods is the absolute limit of NCE&#39;s liability under this express warranty<br />
                                    &nbsp;</p>

                                    <p class="font_7" style="font-size:22px">Contact us</p>

                                    <p class="font_8" style="font-size:18px">If you have any questions regarding this policy, please contact our Head Office or your Account Manager.</p>

                                    <p class="font_8" style="font-size:18px">
                                    <span style="font-weight:bold;">NCE Pty Ltd &nbsp;ACN 105 213 045</span><br />
                                    Address: 34-48 Stanley Drive Somerton VIC Australia 3062<br />
                                    Telephone: 1300 366 024<br />
                                    Email: sales@nce.com.au<br />
                                    Website: www.nce.com.au |</p>

                                    <p class="font_8" style="font-size:18px">
                                    * Estimated resolution days may vary to circumstances beyond ours/ or our suppliers control.&nbsp;<br />
                                    * Refers to calendar days.</p>

                                    <p class="font_8" style="font-size:18px">This Return, Repair and Refund Policy is applicable to purchases made from NCE Pty Ltd. &nbsp;*Estimated resolution days may vary to circumstances beyond ours/ or our suppliers control.&nbsp;
                                    &nbsp;</p>

                                    <p class="font_8" style="font-size:18px">To receive a copy of NCE&rsquo;s full Terms and Conditions please contact our Head Office (03) 9308 7444 or visit <span style="text-decoration:underline"><a href="https://www.nce.com.au/terms-conditions" target="_self">www.nce.com.au/terms-and-conditions</a></span></p>

                                    <p class="font_8" style="font-size:18px">&nbsp;* Refers to calendar days.
                                    &nbsp;</p>

                                    <h5 class="font_5" style="font-size:31px">SPS GROUP PRIVACY POLICY</h5>

                                    <p class="font_8" style="font-size:18px">SPS Corporation Pty Ltd (&ldquo;SPS&rdquo;), and its related companies, including Anton&rsquo;s Mouldings Pty Ltd (&ldquo;Antons&rdquo;), Bellini Moulding Pty Ltd (&ldquo;Bellini&rdquo;), NCE Pty Ltd (&ldquo;NCE&rdquo;), collectively, the &ldquo;SPS Group&rdquo;,recognise that your privacy is very important. We understand that providing your personal information to us requires trust and we take that trust very seriously.</p>

                                    <p class="font_8" style="font-size:18px">The SPS Group complies with the 13 Australian Privacy Principles (APPs) in the Privacy Act 1988 and all relevant privacy laws, including the Health Privacy Principles under state legislation (e.g. those contained in the Health Records Act 2001 (Vic) or the privacy provisions contained in Part 2 of the Health Records (Privacy and Access) Act 1997 (ACT)), the Privacy Regulations 2013 and the Privacy (Credit Reporting) Code.</p>

                                    <p class="font_8" style="font-size:18px">This Privacy Policy explains how SPS Group handles your personal information and ensures that it safeguards your privacy in accordance with the Australian Privacy Principles and other applicable privacy laws. It also explains how our procedures for handling any requests for access to your information or privacy-related complaints.</p>

                                    <p class="font_7" style="font-size:22px">What is personal information?</p>

                                    <p class="font_8" style="font-size:18px">Personal information is any information or an opinion about you that is reasonably capable of identifying you. It includes everyday information (e.g. name, address, phone number, credit card details). It also includes the opinions of others about you that could identify you.</p>

                                    <p class="font_7" style="font-size:22px">What is sensitive information?&nbsp;</p>

                                    <p class="font_8" style="font-size:18px">Sensitive information is a special category of personal information. It is information or an opinion about your:&nbsp;</p>

                                    <ul class="font_8" style="font-size:18px">
                                        <li>
                                        <p class="font_8" style="font-size:18px">Racial or ethnic origin;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">Political opinion;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">Membership of a political association or religious beliefs, affiliations or philosophical beliefs;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">Membership of a professional or trade association or membership of a trade union;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">Sexual preferences or practices;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">Criminal record;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">Health or disability; and</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">Expressed wishes about the future provision of health services.</p>
                                        </li>
                                    </ul>

                                    <p class="font_8" style="font-size:18px">We do not actively seek to collect sensitive information unless it is necessary for our business purposes. If we do have to collect sensitive information, we will do so in accordance with the Australian Privacy Principles, including by obtaining your consent.</p>

                                    <p class="font_7" style="font-size:22px">Collection of personal information&nbsp;</p>

                                    <p class="font_8" style="font-size:18px">We generally collect information from you to make it easier and more rewarding for you to use our products and services. We may collect personal information from you in a variety of scenarios, including (without limitation):</p>

                                    <ul class="font_8" style="font-size:18px">
                                        <li>
                                        <p class="font_8" style="font-size:18px">If you request products or services from us (including via any of the websites of the SPS Group - the &ldquo;Websites&rdquo;), we may collect information such as your name and contact details (ie billing and/or postal address, phone/fax number(s) or email address), date of birth and/or credit card details.</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">If you make an inquiry, provide feedback or make a complaint to us, we may collect your name and contact details.</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">If you are applying for employment with us, we may collect any information that is relevant to such employment including:</p>

                                        <ul>
                                            <li>
                                            <p class="font_8" style="font-size:18px">your name and contact details (ie residential address, phone number(s) and/or email address); and</p>
                                            </li>
                                            <li>
                                            <p class="font_8" style="font-size:18px">any other information relevant to the recruitment process (including any information contained in the application form and your resume).</p>
                                            </li>
                                        </ul>
                                        </li>
                                    </ul>

                                    <p class="font_8" style="font-size:18px">SPS Group may also automatically receive and record information on its server, logs from your browser including your IP address, and the date and time of your access. We use this information for statistical demographic purposes only. We do not access or use personally identifiable information and only treats the data in an anonymous manner.</p>

                                    <p class="font_7" style="font-size:22px">Unsolicited information</p>

                                    <p class="font_8" style="font-size:18px">Where we receive unsolicited information (such as through our Facebook pages or other social media platforms) we will determine, within a reasonable period of time, whether or not we would be permitted to collect the information under the APPs. If it would not be permitted for to collect the information, we will destroy the information or ensure it is de-identified as soon as practicable. Otherwise, we may retain the information in accordance with the terms of this policy.</p>

                                    <p class="font_7" style="font-size:22px">Use of personal information</p>

                                    <p class="font_8" style="font-size:18px">SPS Group will use the personal information it collectsfor the following purposes:</p>

                                    <ul class="font_8" style="font-size:18px">
                                        <li>
                                        <p class="font_8" style="font-size:18px">providing the products and/or services you have requested from SPS Groupincluding for example:</p>

                                        <ul>
                                            <li>
                                            <p class="font_8" style="font-size:18px">to process sales transactions;</p>
                                            </li>
                                            <li>
                                            <p class="font_8" style="font-size:18px">to deliver the products or services to you;</p>
                                            </li>
                                            <li>
                                            <p class="font_8" style="font-size:18px">to register you for a service requested by you, such as our newsletter(s) and e-newsletters, mail outs, rewards program(s) or competitions and administering such services;</p>
                                            </li>
                                            <li>
                                            <p class="font_8" style="font-size:18px">to manage warranty claims;</p>
                                            </li>
                                            <li>
                                            <p class="font_8" style="font-size:18px">to respond to any inquiries, feedback or complaints made by you; and</p>
                                            </li>
                                        </ul>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">direct marketing of products and services which we believe may interest you, including product updates and developments, special events or promotions;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">assisting us to improve our products and services and making them more relevant to you;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">assisting us to improve our Websites or Facebook pages;.</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">processing and assessing employment applications for current and future positions; and</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">otherwise managing our internal business operations and processes.</p>
                                        </li>
                                    </ul>

                                    <p class="font_7" style="font-size:22px">Direct marketing</p>

                                    <p class="font_8" style="font-size:18px">SPS Group will only use your personal information for the purpose of direct marketing activities where we have obtained your consent to do so, or in circumstances where we have collected the information directly from you and you would reasonably expect that your personal information would be used or disclosed for this purpose. We may also share your personal information with our related entities, so that they can provide you directly with marketing material about their products and services.</p>

                                    <p class="font_8" style="font-size:18px">We will provide you with the opportunity to &ldquo;opt out&rdquo; of receiving marketing materials at the time of collection and/or at any time afterwards by either unsubscribing from the email service or contacting our Privacy Officer via the contact details provided below.</p>

                                    <p class="font_7" style="font-size:22px">Disclosure of personal information</p>

                                    <p class="font_8" style="font-size:18px">There will be occasions where it will be necessary for the SPS Group to disclose your personal information to third parties. For example, the SPS Group may disclose your personal information to:</p>

                                    <ul class="font_8" style="font-size:18px">
                                        <li>
                                        <p class="font_8" style="font-size:18px">contractors and third party service providers on a confidential basis that we use in the ordinary course of our business to assist with the delivery of the product or service. This includes organisations such as marketing agencies, data processing companies, printing and mailing houses, delivery companies, or finance agencies or debt collection agencies;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">government authorities or other third parties as required by law; or</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">any other purpose that you have consented to.</p>
                                        </li>
                                    </ul>

                                   <p class="font_8" style="font-size:18px">The SPS Group will generally not disclose your personal information to an organisation or individual outside Australia unless, generally, the country to which it is being disclosed has a similar level of privacy protection, or you, or your authorised or legal representative, has consented to the disclosure. If we do disclose your personal information overseas, we will also take adequate measures to ensure that the personal information is handled by the overseas recipient in accordance with the Privacy Act 1988 (Cth) and our instructions for the purposes described above. Personal information you provide to us via the Websites will be securely stored in a combination of electronic and hard copy files. Some electronic information is stored securely Canada and the USA by k-e commerce.</p>

                                    <p class="font_7" style="font-size:22px">Security of information collected</p>

                                    <p class="font_8" style="font-size:18px">SPS Group will endeavour to protect the confidentiality of your personal information, account information and personal communications. We use appropriate encryption software to protect your information submitted via the Websites from misuse, loss and unauthorised access or modification.</p>

                                    <p class="font_8" style="font-size:18px">All personal information (other than credit card information) that is submitted for processing is confined to the offices of SPS Group. Only employees who need to access the information and/or data to perform a specific job (such as invoicing, customer service, order processing) are granted access to personally identifiable information.Unfortunately, no data transmission over the Internet can be guaranteed to be totally secure. While we endeavour to prevent your personal information from misuse or unauthorised access, we cannot guarantee the security of any information you transmit to us or receive from us. As you conduct this activity at your own risk, SPS Group accepts no responsibility for misuse of your personal data which arise from unauthorised access.</p>

                                    <p class="font_7" style="font-size:22px">Cookies</p>

                                    <p class="font_8" style="font-size:18px">Our Websites use cookies. A &ldquo;cookie&rdquo; is a small file placed on a customer&rsquo;s computer or device which lets our site store information. This information allows us to make our Websites easier to use. Cookies allow our servers to keep track of customer details between visits to our Websites. This information stored by the cookie includes the data that is provided during online registration. This information is only accessible online when a customer enters their username and password. Other information stored by the cookie includes website traffic data. This is not used to identify individual details but only collated into aggregate anonymous results in order to evaluate and improve our shopping service for our customers.</p>

                                    <p class="font_7" style="font-size:22px">Credit information</p>

                                    <p class="font_8" style="font-size:18px">The SPS Group may provide &lsquo;commercial credit&rsquo; for the purposes of the Privacy Act to businesses who apply for credit in relation to our products and services. &quot;Credit information&quot; relates mainly to your credit-related dealings with us and comprises various types of information collected by credit reporting bodies (&quot;CRBs&quot;) that report on consumer credit worthiness. We may collect or generate various categories of credit information about you. We may also collect &quot;credit eligibility information&quot; about you, which is mostly information provided by CRBs relating to your dealings with other credit providers (for example, financial institutions that provide you with loans or other businesses that provide you with credit in connection with their products or services). &quot;Credit information&quot; and &quot;credit eligibility information&quot; may include:</p>

                                    <ul class="font_8" style="font-size:18px">
                                        <li>
                                        <p class="font_8" style="font-size:18px">identification information: such as your name, address, date of birth or employer;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">consumer credit liability information: being information about consumer credit accounts you hold with other credit providers;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">details about information requests made to CRBs: such as the fact that we or another credit provider have requested credit reporting information about you from a CRB to assess a credit application and various details about the credit you have applied for;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">default information: being information about overdue payments owed by you in connection with consumer credit which have been disclosed to a CRB by other credit providers;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">payment information: being information that an overdue payment has been repaid;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">information about consumer credit-related serious credit infringements;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">new arrangement information: being information about certain credit-related arrangements you may have entered with another credit provider in connection with a consumer credit default or serious credit infringement;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">court proceedings information: being information about certain credit-related judgments;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">personal insolvency information: being information recorded in the National Personal Insolvency Index about bankruptcy or various other insolvency-related matters;</p>
                                        </li>
                                        <li>
                                        <p class="font_8" style="font-size:18px">publicly available information about activities relating to credit worthiness.</p>
                                        </li>
                                    </ul>

                                    <p class="font_8" style="font-size:18px">Credit eligibility information also includes creditworthiness information that we may derive from data we receive from a CRB, such as a credit risk score. We may collect credit information about you in any of the circumstances described above under the heading &quot;How we collect your personal information&quot; above. We collect credit eligibility information from CRBs but may collect it from other third parties where permitted by the Privacy Act (such as from other credit providers with your consent).</p>

                                    <p class="font_7" style="font-size:22px">Linked Sites</p>

                                    <p class="font_8" style="font-size:18px">External sites that are linked to or from our Websites are not under our control and you are advised to review their Privacy Policies. Users should note there are inherent risks associated with the transmission of information via the Internet and you should therefore make your own assessment of the potential risk to the security of your information.</p>

                                    <p class="font_7" style="font-size:22px">Are you under 18?</p>

                                    <p class="font_8" style="font-size:18px">We do not want to collect personal information from anyone under the age of 18. If you are under 18, you should not enter any information on this Website.</p>

                                    <p class="font_7" style="font-size:22px">Access and correction</p>

                                    <p class="font_8" style="font-size:18px">You may request access to your personal information held by the SPS Group by writing to our Privacy Officer at <span style="text-decoration:underline;"><a href="mailto:privacy@spsgroup.com.au" target="_self">privacy@spsgroup.com.au</a></span>. We may ask to verify your identity and for more information about your request. Where we are legally permitted to do so, we may refuse your request and give you reasons for doing so. Where you request your personal information to be updated and there is a dispute about the facts, we will make a note on your personal information of such dispute.</p>

                                    <p class="font_8" style="font-size:18px">We reserve the right to charge a reasonable administrative fee for access and updating requests.</p>

                                    <p class="font_7" style="font-size:22px">Complaints</p>

                                    <p class="font_8" style="font-size:18px">If you have any questions in relation to your privacy or wish to make an access request or a privacy complaint, please email our Privacy Officer at <span style="text-decoration:underline;"><a href="mailto:privacy@spsgroup.com.au" target="_self">privacy@spsgroup.com.au</a></span>. Our Privacy Officer will contact you within a reasonable time after receipt of your request or complaint.</p>

                                    <p class="font_8" style="font-size:18px"><span class="wixGuard">​</span></p>

                                    <p class="font_8" style="font-size:18px">General information about privacy may be found at: <span style="text-decoration:underline;"><a href="http://www.privacy.gov.au/" target="_blank" rel="noopener">http://www.privacy.gov.au/</a></span> and <span style="text-decoration:underline;"><a href="http://www.oaic.gov.au/" target="_blank" rel="noopener">http://www.oaic.gov.au/</a></span>.</p>

                                    <p class="font_7" style="font-size:22px">Changes to our Privacy Policy</p>

                                    <p class="font_8" style="font-size:18px">If at any time our Privacy Policy changes, the updated details will be available on our Websites for your perusal.
                                    </p>
                            </div>
                        </div>
              <!--<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>-->
            </div>
      </div>
</div>
<!-- Modal for Privacy Policy -->
<div class="modal fade" id="TermsModalLong" tabindex="-1" role="dialog" aria-labelledby="TermsModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
            <div class="modal-content">
                  <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Terms and Conditions</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                  </div>
                  <div class="modal-body">
                        <div class="modal-body__wrap">


                            <div role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#generalTab" aria-controls="generalTab" role="tab" data-toggle="tab">General T&C's</a>

                                        </li>
                                        <li role="presentation"><a href="#competitionTab" aria-controls="competitionTab" role="tab" data-toggle="tab">Competition T&C's</a>

                                        </li>
                                    </ul>
                                     <!-- Nav tabs  end-->
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="generalTab">

                                            <h5 class="font_5" style="font-size:31px"><span class="color_14">NCE PTY LTD ACN 105 213 045<br /> GENERAL TERMS AND CONDITIONS OF TRADE</span></h5>

                                            <p class="font_8" style="font-size:18px">In these terms and conditions the Supplier means NCE Pty Ltd ACN 105 213 045, a company duly incorporated in the State of Victoria and having its offices situated at 34-48 Stanley Drive, Somerton in the State of Victoria, and the Customer means the purchaser whose details are set out in Parts 1 and 2 or Parts 1 and 3 of the attached Credit Application Form or a person or entity whose order for the purchase of the Supplier&#39;s goods and/or services is accepted by the Supplier.</p>

                                           <h5 class="font_5" style="font-size:31px"><span class="color_14">Acceptance of Customer&#39;s Order</span></h5>


                                            <p class="font_8" style="font-size:18px">These terms and conditions apply to every order for goods and/or services (&quot;Order&quot;) between the Supplier and the Customer and any terms and conditions of the Customer&#39;s Order deviating from or inconsistent with these terms and conditions are expressly excluded, obviated and rejected by the Supplier. &nbsp;This exclusion and rejection includes any statement by the Customer that the Customer&#39;s terms and conditions shall prevail notwithstanding any stipulation by the Customer regarding the manner of declaring such rejection. &nbsp;A contract is only concluded between the Supplier and Customer for the supply of goods and/or services when the Order has been accepted by the Supplier. &nbsp;The Supplier reserves its right in its unfettered discretion to accept all or part of the Order. The terms of this Clause apply to every quotation or offer by the Supplier for the supply of goods and/or services.</p>

                                            <p class="font_7" style="font-size:22px">Prices</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;The Supplier&#39;s quotation may be exercised within 14 days from the date of issue.</p>

                                            <p class="font_8" style="font-size:18px">(b) &nbsp;A quotation is made on the basis of the Supplier&#39;s terms and conditions at the date of issue. &nbsp;In the event of any variation (other than as directed or approved by the Supplier) the Supplier reserves the right to amend or withdraw this quotation.</p>

                                            <p class="font_8" style="font-size:18px">(c) &nbsp;All prices are in Australian ($) dollars unless stated otherwise.</p>

                                            <p class="font_8" style="font-size:18px">(d) &nbsp;All prices quoted and all tax invoices are submitted as exclusive of GST.</p>

                                            <p class="font_8" style="font-size:18px">(e) &nbsp;The Supplier reserves the right to vary the quoted price should there be any adjustment necessary attributable to any cause beyond the Supplier&#39;s control including but not limited to variation in indirect taxes and/or government actions. &nbsp;The Supplier will use its best endeavours to notify the Customer of any such variations before provision of the goods and/or services.</p>

                                            <p class="font_8" style="font-size:18px">(f) &nbsp;Where the Supplier publishes or discloses a price list, this list is an invitation to treat only and the Supplier reserves the right to accept or reject in its absolute and unfettered discretion all or part of any Orders which may be received by it. Any price list of the Supplier is subject to alteration at any time without notice. In addition, the Supplier reserves the right to set a minimum invoice value of $399.00 (excluding GST) on any Order.</p>

                                            <p class="font_7" style="font-size:22px">Property &amp; Title</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;The risk in any goods and/or services sold pass to the Customer when all or part of the goods and/or services are delivered to the premises of the Customer whether by carrier employed or engaged by the Supplier or the Customer. &nbsp;Notwithstanding anything contained herein, property in and legal title to the goods and/or services does not pass to the Customer until payment for all debts owing to the Supplier by the Customer has been received by the Supplier in cleared funds. &nbsp;Until such payment has been received by the Supplier, the Customer will store the goods and/or services separately and apart from its own goods and/or services and those of any other person or company. All intellectual property subsisting in or created pursuant to the Supplier providing any goods and/or services vests in the Supplier as and when created.</p>


                                            <p class="font_8" style="font-size:18px">(b) &nbsp;The Customer may re-sell any of the goods and/or services on normal commercial terms before the Supplier is paid in full provided that: (i) &nbsp;the Customer re-sells as principal and has no right to commit the Supplier to any contractual relationship or liability to any third party; and (ii) subject to (i) above, as between the Supplier and the Customer, the Customer re-sells as fiduciary agent and bailee of the Supplier; and (iii) the Customer holds all rights in respect of the re-sale proceeds on behalf of the Supplier and, on request of the Supplier, will assign any claim against any such third party for any unpaid debt and for this purpose the Customer irrevocably appoints the directors of the Supplier for the time being as joint and several attorneys of the Customer to sign any documents to give effect to such assignment; and (iv) the Customer holds the proceeds of any re-sale or insurance claim on trust for the Supplier until the Supplier has been paid in full for those goods and/or services, which are the subject of the re-sale or insured loss.</p>


                                            <p class="font_8" style="font-size:18px">(c) &nbsp;Until payment in full of all debts owing to the Supplier by the Customer, the Supplier is entitled, at its discretion, without further notice and without prejudice to any other of its rights to re-take possession of the goods and/or services delivered and re-sell them, or any of them, and may enter upon the Customer&#39;s premises, by its servants or agents, for that purpose, without any liability on the part of the Supplier or its servants or agents for any loss or damage suffered as a consequence of such entry or re-taking of possession and the Customer hereby agrees to provide the Supplier or its servants or agents with an irrevocable licence to so enter any premises occupied by it if:<br />
                                            (i)&nbsp;there is a breach of any term of any contract between the Supplier and the Customer; or (ii) the Customer has provided any false or misleading information to the Supplier including information set out in any application for credit or to open an account with the Supplier; or (iii) the Customer commences to be wound up or bankrupted or an order in bankruptcy/sequestration order is made or the Customer is placed in liquidation, under official management, or a receiver, or a receiver and manager or voluntary administrator is appointed in respect of the Customer, its undertaking or property or any part thereof, or an encumbrance, by itself or by an agent, takes or purports to take possession of the Customer&#39;s undertaking or property or any part thereof; or (iv) the Customer parts with possession of the goods and/or services or any of them otherwise than by way of sale in the ordinary course of its business.</p>


                                            <p class="font_8" style="font-size:18px">(d) &nbsp;Notwithstanding the foregoing, the Customer shall be responsible for the goods and/or services until payment in full is made to and received by the Supplier, and the Customer shall indemnify the Supplier for any damage, destruction, depreciation and diminution in value of the goods and/or services during the period the Customer is responsible for the goods and/or services</p>


                                            <p class="font_8" style="font-size:18px">(e) &nbsp;These provisions apply despite any arrangement under which the Supplier provides credit to the Customer and these provisions will prevail to the extent of any inconsistency between these provisions and any other agreement or arrangement entered into by the Customer and the Supplier. &nbsp;In addition, the Supplier may recover the purchase price for the goods and/or services provided to the Customer in respect of any Order and may commence legal proceedings and may file an application for the appointment of a liquidator to the Customer notwithstanding that property in the goods and/or services under any Order has not passed to the Customer.</p>


                                            <p class="font_8" style="font-size:18px">(f) &nbsp;By assenting to these terms and conditions the Customer acknowledges and agrees as follows: (i) these terms and conditions constitute a security agreement and in particular the retention of title arrangement referred to in Clause 3(a) constitutes a purchase money security interest for the purposes of the Personal Property Securities Act 2009 (Cth) (&ldquo;PPSA&rdquo;); and (ii) a security interest is taken in all goods and/or services previously supplied by the Supplier to the Customer (if any) and all goods and/or services that will be supplied in the future by the Supplier to the Customer during the continuance of the parties&rsquo; relationship.</p>


                                            <p class="font_8" style="font-size:18px">(g) &nbsp;The Customer undertakes to: (i) sign any further documents and provide any further information (including serial numbers and being complete, accurate and up to date in all respects) which the Supplier may reasonably require to register a financing statement or financing change statement on the Personal Property Securities Register (&ldquo;PPSR&rdquo;) established by the PPSA; (ii) indemnify, and upon demand reimburse, the Supplier for all expenses incurred in preparing, maintaining and registering a financing statement or financing change statement on the PPSR or releasing any collateral charged thereby: (iii) not register a financing change statement or an amendment demand pursuant to the PPSA without the Supplier&rsquo;s prior written consent; and (iv) give the Supplier not less than 14 days prior written notice of any proposed change to the Customer&rsquo;s name or any other change in the Customer&rsquo;s details (including, but not limited to, changes in the Customer&rsquo;s address, contact numbers or business practices);</p>


                                            <p class="font_8" style="font-size:18px">(h) &nbsp;Unless otherwise agreed in writing by the Supplier the Customer waives its right to receive a verification statement in accordance with section 157 of the PPSA. (i) If Chapter 4 of the PPSA would otherwise apply to the enforcement of these terms and conditions as a security agreement, the Customer agrees that none of the provisions specified in Section 115 of the PPSA will apply to the enforcement of these terms and conditions and waives any requirement by the Supplier to comply with any of those provisions.</p>


                                            <p class="font_7" style="font-size:22px">Insurance</p>

                                            <p class="font_8" style="font-size:18px">The Supplier agrees at its own cost, to insure the goods or otherwise self insure the goods, in the Supplier&#39;s name, against such risks as a prudent owner of the goods would insure for their full insurable value, notwithstanding any contrary operation of clause 3 herein.</p>


                                            <p class="font_7" style="font-size:22px">Payment</p>

                                            <p class="font_8" style="font-size:18px">The Customer agrees to and will pay in accordance with the tax invoice rendered by the Supplier namely either by:</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;payment in full prior to delivery of the goods and/or services under any Order;</p>

                                            <p class="font_8" style="font-size:18px">(b) &nbsp;cash on delivery; or</p>

                                            <p class="font_8" style="font-size:18px">(c) &nbsp;terms as stated.</p>

                                            <p class="font_8" style="font-size:18px">If the Supplier extends trading terms to the Customer, payment for all goods and/or services under any Order provided will be one of the following:</p>

                                            <p class="font_8" style="font-size:18px">(ai) &nbsp;within thirty (30) days after the end of the month of the date of tax invoice;</p>

                                            <p class="font_8" style="font-size:18px">(bi) &nbsp;within forty five (45) days after the end of the month of the date of tax invoice;</p>

                                            <p class="font_8" style="font-size:18px">(ci) &nbsp;within sixty (60) days after the end of the month of the date of tax invoice;</p>

                                            <p class="font_8" style="font-size:18px">(di) &nbsp;electronic funds transfer within seven (7) days after the end of the month of the date of tax invoice; or</p>

                                            <p class="font_8" style="font-size:18px">(ei) &nbsp;electronic funds transfer within fourteen (14) days after the end of the month of the date of tax invoice.&nbsp;</p>

                                            <p class="font_8" style="font-size:18px">Interest is payable by the Customer, immediately on demand by the Supplier, on all amounts overdue to the Supplier from the date of provision of goods and/or services until payment at the rate of eighteen (18) per centum per annum, however all interest charges will be waived by the Supplier in the event that payment is made to the Supplier within the time stipulated by the Supplier. &nbsp;Where payment is not made by the due date, the Customer shall, in addition to any other obligations imposed hereunder, pay to the Supplier on demand all costs of the Supplier (including but not limited to storage, delivery, collection, obsolescence and legal costs on a full indemnity basis).</p>


                                            <p class="font_8" style="font-size:18px">All payments received by the Supplier shall be applied as follows:</p>

                                            <p class="font_8" style="font-size:18px">(aii) &nbsp;firstly, towards any costs of the Supplier referred to above (or any part thereof);</p>

                                            <p class="font_8" style="font-size:18px">(bii) &nbsp;secondly, towards any interest payable as set out above (or any part thereof); and</p>

                                            <p class="font_8" style="font-size:18px">(cii) &nbsp;thirdly, towards any other amounts payable by the Customer to the Supplier.</p>

                                            <p class="font_8" style="font-size:18px">Time of payment in accordance with this clause 5 for any goods and/or services under any Order provided to the Customer is an essential term of any Order between the Supplier and the Customer.</p>

                                            <p class="font_7" style="font-size:22px">Cancellation</p>

                                            <p class="font_8" style="font-size:18px">Cancellation of any Order between the Customer and the Supplier requires prior approval in writing from the Supplier otherwise the Order will be fulfilled and the Supplier will be entitled to payment in full from the Customer. &nbsp;In particular, the Supplier shall not accept any cancellation of any Order where the Supplier has made or cut up any product of the Order.</p>

                                            <p class="font_8" style="font-size:18px"><span class="wixGuard">​</span></p>

                                            <p class="font_8" style="font-size:18px">The Supplier is not obliged to supply any goods and/or services in relation to any Order and may cancel any Order (or part thereof) at any time if:</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;there is a breach of any term of any agreement between the Supplier and the Customer; or</p>

                                            <p class="font_8" style="font-size:18px">(b) &nbsp;the Customer has provided any false or misleading information to the Supplier including information set out in any application for credit or to open an account with the Supplier; or</p>

                                            <p class="font_8" style="font-size:18px">(c) &nbsp;the Customer commences to be wound up or bankrupted or an order in bankruptcy/sequestration order is made or the Customer is placed in liquidation, under official management, or a receiver, or a receiver and manager or voluntary administrator is appointed in respect of the Customer, its undertaking or property or any part thereof, or an encumbrance, by itself or by an agent, takes or purports to take possession of the Customer&#39;s undertaking or property or any part thereof; or</p>

                                            <p class="font_8" style="font-size:18px"><span class="wixGuard">​</span></p>

                                            <p class="font_8" style="font-size:18px">(d) &nbsp;the Supplier is unable to supply the goods and/or services in relation to any Order as a result of the failure of any supplier of the Supplier to provide goods and/or services which are required in order for the Supplier to provide the goods and/or services to the Customer.</p>

                                            <p class="font_7" style="font-size:22px">Claims</p>

                                            <p class="font_8" style="font-size:18px">Acceptance of the goods and/or services delivered shall be deemed for all purposes to have taken place at the expiration of thirty (30) days from the date of each delivery. &nbsp;No goods and/or services will be accepted for return unless agreed in writing by the Supplier prior to such return and then only upon conditions acceptable to the Supplier and at the Customer&#39;s entire risk as to loss or damage and provided the goods and/or services are and remain sealed in the manner in which they were delivered. Where the Supplier agrees to accept goods for return a restocking charge of 25% of the price of the goods returned shall be paid by the Customer and the Customer shall remain responsible for all freight charges upon return. The Supplier&#39;s liability for a breach of any conditions or warranty implied by Division 2 of Part V of the Trade Practices Act 1974 (other than a condition or warranty implied by Section 69 of the Act) is limited to such one or more of the following as the Supplier decides:</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;the replacement of the goods and/or services or the supply of equivalent goods and/or services; or</p>

                                            <p class="font_8" style="font-size:18px">(b) &nbsp;the repair of the goods; or</p>

                                            <p class="font_8" style="font-size:18px">(c) &nbsp;the payment of the cost of replacing the goods and/or services or of acquiring equivalent goods and/or services; or</p>

                                            <p class="font_8" style="font-size:18px">(d) &nbsp;the payment of the cost of having the goods repaired.</p>

                                            <p class="font_8" style="font-size:18px">Without limiting the generality of any other provision of these terms and conditions but subject to the above, the Supplier is not under any liability to the Customer or to any other person in respect of any loss or damage (including consequential loss or damage) however caused, which may be suffered or incurred or which may arise either directly or indirectly in respect of the supply of the goods and/or services or any ancillary services or advice or the failure or omission on the part of the Supplier to comply with its obligations hereunder.</p>

                                            <p class="font_8" style="font-size:18px">Except as expressly provided to the contrary in these terms and conditions, all terms, conditions, warranties, undertakings, inducements or representations whether express, implied, statutory or otherwise are excluded to the extent permitted by law, including but not limited to the United Nations Convention on Contracts for the International Sale of Goods.</p>

                                            <p class="font_7" style="font-size:22px">Force Majeure</p>

                                            <p class="font_8" style="font-size:18px">If delivery is prevented or delayed, in part or all, by reason of an Act of God, or the consequence thereof including, but not limited to fire, flood, typhoon, earthquakes, or by reason of riots, wars, hostilities, terrorism, government restrictions, trade embargoes, strikes, lockouts, labour disputes, boycotting of goods, ship shortage, delays or damage in transportation or other causes beyond the Supplier&#39;s control, the Supplier may, at its sole and unfettered option, perform the agreement &nbsp;or the unfulfilled portion thereof within a reasonable time from the removal of the cause preventing or delaying performance, or rescind unconditionally and without liability, this agreement or the unfulfilled portion thereof.</p>

                                            <p class="font_7" style="font-size:22px">Delivery</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;Any dates specified for delivery of any Order are estimated dates only and the Supplier shall not be liable for any damage or loss which the Customer may suffer as a result of the provision of goods and/or services being delayed beyond such dates for any reason whatsoever.</p>

                                            <p class="font_8" style="font-size:18px">(b) &nbsp;The Supplier may deliver any Order by way of two or more instalments.</p>

                                            <p class="font_7" style="font-size:22px">Warranty</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;Goods sold under any Order shall have the benefit of any warranty given by the manufacturer and will only be considered for acceptance by the Supplier if return of the goods or any part of them is in accordance with the Supplier&#39;s warranty policy but the Supplier shall not be liable for any loss or damage either direct or consequential arising out of any defects arising from the use of the goods. &nbsp;Any modification, alteration or variation of any goods by the Customer requires the prior written approval in writing from the Supplier otherwise any applicable warranty is rendered immediately void and the Customer will have no claim against the Supplier under any applicable warranty.</p>


                                            <p class="font_8" style="font-size:18px">(b) &nbsp;Without limiting or restricting any statutory or implied warranties or consumer guarantees that may apply to goods and/or services sold under any Order where the Order includes used or second hand goods the Customer acknowledges and agrees that: (i) the goods are acquired on an &quot;as is&quot; basis and whilst fit for the purpose for which the goods are intended to be commonly used, are not free from the specified defects which the Supplier has notified to the Customer prior to accepting the Order; (ii) the goods have been inspected by the Customer prior to placing the Order and have been determined by the Customer as being fit for their intended purpose; and (iii) the Customer has not disclosed to the Supplier any other purpose for which the goods are intended to be used.</p>

                                            <p class="font_7" style="font-size:22px">Clerical Errors</p>

                                            <p class="font_8" style="font-size:18px">Clerical errors, typing errors or other errors in computations, catalogue, quotation, acceptance, offer, invoice, delivery docket, credit note or specification of the Supplier shall be subject to correction by the Supplier.</p>


                                            <p class="font_7" style="font-size:22px">Modification</p>

                                            <p class="font_8" style="font-size:18px">All modifications and amendments to these provisions or any approvals hereunder shall be in writing by a duly authorised signatory, and if otherwise, shall not be binding upon the Supplier.</p>

                                            <p class="font_7" style="font-size:22px">GST</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;For the purpose of this clause:</p>

                                            <p class="font_8" style="font-size:18px">GST means GST within the meaning of the GST Act.</p>

                                            <p class="font_8" style="font-size:18px">GST Act means A New Tax System (Goods and Services Tax) Act (Cth) 1999 (as amended).</p>

                                            <p class="font_8" style="font-size:18px">Except where the contrary intention appears, expressions used in this condition and in the GST Act have the meanings given to them in the GST Act.</p>


                                            <p class="font_8" style="font-size:18px">(b) &nbsp;If the introduction of GST is associated with the abolition or reduction of any tax, duty, excise or statutory charge which directly or indirectly affects the net dollar margin of a supplier in respect of any supply made under this document, the consideration (excluding GST) payable for the supply must be varied so that the Supplier&#39;s net dollar margin in respect of the supply remains the same.</p>

                                            <p class="font_8" style="font-size:18px">(c) &nbsp;Except as provided in Clause 13(b) and where express provision is made to the contrary, the consideration payable by a party under this document represents the value of the supply for which payment is to be made.</p>

                                            <p class="font_8" style="font-size:18px">(d) &nbsp;If this document requires a party to pay for, reimburse or indemnify against any expense or liability (&quot;reimbursable expense&quot;) incurred by the other party (&quot;payee&quot;) to a third party, the amount to be paid, reimbursed or indemnified is the amount of the reimbursable expense net of any input tax credit to which the payee is entitled in respect of the reimbursable expense (&quot;net expense&quot;).</p>

                                            <p class="font_8" style="font-size:18px">(e) &nbsp;Subject to Clause 13(f), if a party makes a taxable supply under this document for a consideration which represents its value by virtue of Clause 13(c) or the net expense by virtue of Clause 13(d), then the party liable to pay for the taxable supply must also pay the amount of any GST payable in respect of the taxable supply at the time the consideration for the taxable supply is payable.</p>

                                            <p class="font_8" style="font-size:18px">(f) &nbsp;A party is not obliged under Clause 13(e) to pay the GST on a taxable supply to it, until that party is given a valid tax invoice for the supply.</p>

                                            <p class="font_8" style="font-size:18px">(g) &nbsp;If the amount of GST paid or payable by the Supplier on any supply made under this document differs from the amount paid by the Supplier as GST, due to an adjustment of the value of the taxable supply for the purpose of calculating GST, then the amount paid as GST by the Customer must be adjusted by a payment by the Customer &nbsp;to the Supplier or by the Supplier to the recipient, as the case requires, so that the amount paid by the recipient as GST accurately represents the GST payable in respect of the supply.</p>

                                            <p class="font_7" style="font-size:22px">Jurisdiction</p>

                                            <p class="font_8" style="font-size:18px">All contracts (including any Order) between the Supplier and the Customer shall be governed by the laws of the State of Victoria and the parties shall submit to the non-exclusive jurisdiction of the courts of the State of Victoria (and any courts which can hear appeals from such courts).</p>

                                            <p class="font_7" style="font-size:22px">Execution</p>

                                            <p class="font_8" style="font-size:18px">Any contract (including any Order) between the Supplier and the Customer may be executed on behalf of the Customer by any agent or employee of the Customer and the Customer shall be bound by these terms and conditions irrespective of whether any such execution was unauthorised or fraudulent.</p>

                                            <p class="font_7" style="font-size:22px">Indemnification</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;The Customer acknowledges and agrees to defend, indemnify and hold harmless the Supplier from and against any and all claims, actions, demands, proceedings, suits, penalties, fines, judgements, costs, losses, damages, omissions, injuries and expenses, including legal fees (on a full indemnity basis) and expenses, which are related to, in connection with or arise out of or incidental to the provision by the Customer to the Supplier of any designs, drawings, sketches, plans, photographs, prototypes, instructions, specifications or any information relied upon by the Supplier in the provision of the goods and/or services or any other means howsoever described including but not limited to electronic communication, for the purposes of the Supplier utilising same as an aid, assistance or otherwise in the provision of the goods and/or services to the Customer.</p>

                                            <p class="font_8" style="font-size:18px">(b) &nbsp;In the event that the Customer neglects and/or fails and/or refuses to defend, indemnify or hold harmless the Supplier, the Customer acknowledges and agrees: (i) to being joined by the Supplier as a party to any proceeding as contained in Condition 16(a) initiated against the Supplier; (ii) to pay to the Supplier all legal and other costs (on a full indemnity basis) associated or in connection with the joinder of the Customer; (iii) to waive and forever abandon any rights which the Customer may have against the Supplier to claim or seek payment of any costs orders which may be made against the Supplier in favour of the Customer in connection with any proceedings initiated as a consequence of Condition 16(a) herein.</p>

                                            <p class="font_7" style="font-size:22px">Special Orders</p>

                                            <p class="font_8" style="font-size:18px">Where the Customer places an Order in writing with the Supplier for non-stock items, the Supplier will not accept return of non-stock items unless the manufacturer agrees to accept return from the Supplier. &nbsp;The Supplier may deduct transport, insurance, handling ,restocking charges and return freight charges from the credit due to the Customer where any such items are returned to the Supplier and their return has been accepted by the Supplier in writing.</p>

                                            <p class="font_7" style="font-size:22px">Substitution</p>

                                            <p class="font_8" style="font-size:18px">The Supplier reserves the right to substitute some other make or brand with similar specifications if any item ordered by the Customer is not available. &nbsp;The Customer is deemed to have accepted substitution where it does not object to same within seven (7) days of the date of delivery of the goods (unless a longer period is imposed by law). &nbsp;If the Customer is not satisfied with the substituted goods, the goods may be returned to the Supplier for credit, subject to any deductions made by the Supplier on account of return freight charges and any other charges imposed by these terms.</p>

                                            <p class="font_7" style="font-size:22px">Catalogue/Website and Samples</p>

                                            <p class="font_8" style="font-size:18px">The Customer acknowledges, agrees and accepts that there may be variations in the colour and/or finish and/or texture of any actual products supplied to the Customer:</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;from those as appearing in the Supplier&#39;s catalogue;</p>

                                            <p class="font_8" style="font-size:18px">(b) &nbsp;from those as appearing in the Supplier&#39;s website; or</p>

                                            <p class="font_8" style="font-size:18px">(c) &nbsp;from any samples held by the Supplier.</p>

                                            <p class="font_7" style="font-size:22px">Assignment</p>

                                            <p class="font_8" style="font-size:18px">The Customer may only assign its rights in relation to any Order subject to the prior written consent of the Supplier.</p>

                                            <p class="font_7" style="font-size:22px">Severability</p>

                                            <p class="font_8" style="font-size:18px">Each supply made by the Supplier will be made under a separate contract and will be invoiced separately. Each invoice will be payable by the Customer in full, in accordance with the specified terms of payment, without reference to and despite any default in any supply covered by any other invoice or Order.</p>

                                            <p class="font_7" style="font-size:22px">Liability</p>

                                            <p class="font_8" style="font-size:18px">The Supplier is not liable for, and the Customer may not claim for, any loss or damage suffered by the Customer whether in contract or tort resulting from a breach of these terms and conditions in relation to any Order, or the non-performance of any goods and/or services, the aggregate amount of which is limited to the value of the goods and/or services as detailed in the relevant tax invoice from the Supplier.</p>

                                            <p class="font_7" style="font-size:22px">Special Conditions</p>

                                            <p class="font_8" style="font-size:18px">Any special terms of supply by the Supplier of the goods and/or services are by this reference incorporated herein with the same force and effect as those set forth herein in full and shall prevail to the extent of any inconsistency with the printed terms and conditions hereof.</p>

                                            <p class="font_7" style="font-size:22px">Interpretation</p>

                                            <p class="font_8" style="font-size:18px">In these terms and conditions, unless the contrary intention appears:</p>

                                            <p class="font_8" style="font-size:18px">(a) &nbsp;a reference to a person (including a party) includes an individual, a firm, a body corporate, a partnership, joint venture, an unincorporated body or association or any government agency;</p>

                                            <p class="font_8" style="font-size:18px">(b) &nbsp;a reference to a particular person includes a reference to the person&#39;s executors, administrators, successors, substitutes (including persons taking by novation) and assigns;</p>

                                            <p class="font_8" style="font-size:18px">(c) &nbsp;a reference to a document (including these terms and conditions) includes any variation of it;</p>

                                            <p class="font_8" style="font-size:18px"><br />
                                            (d) &nbsp;the singular includes the plural and vice versa;</p>

                                            <p class="font_8" style="font-size:18px">(e) &nbsp;the words &quot;include&quot;, &quot;including&quot; or &quot;such as&quot; are not used as, nor are they to be interpreted as words of limitation;</p>

                                            <p class="font_8" style="font-size:18px">(f) &nbsp;if a payment or other act must (but for this clause) be made or done on a day which is not a business day in Melbourne, then it must be made or done on the next business day in Melbourne;</p>


                                            <p class="font_8" style="font-size:18px">(g) &nbsp;this document must not be construed adversely to a party solely because that party was responsible for preparing it.
                                            </p>
                                        </div>




                                        <div role="tabpanel" class="tab-pane" id="competitionTab">


                                                <h5 class="font_5" style="font-size:31px"><span class="color_14">NCE PTY LTD ACN 105 213 045<br /> TERMS AND CONDITIONS OF COMPETITIONS ON NCE.COM.AU</span></h5>

                                                    <ol class="font_8" style="font-size:18px">
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Instructions on how to enter and prize information forms part of these Terms and Conditions. Participation in any competition is deemed as acceptance of these Terms and Conditions.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">The promoter is NCE Pty Ltd (ACN 105 213 045) of 34-48 Stanley Drive, Somerton 3062 (Promoter).</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">NCE standardly conducts two types of competition, games of skill or game of chance. In the case of game of skill, each entry will be individually judged based on its literary and creative merit. In the case of game of chance, winners are based on random selection.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">When there is a game of chance, NCE operates in accordance with the Victorian Commission for Gambling and Liquor Regulation (VCGLR) regulations.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Entry is open to residents of Australia aged eighteen years or over only. Employees of the Promoter and their immediate families, suppliers, associated companies and agencies are ineligible to enter. Persons under the age of 18 years are not considered adults and therefore are not able to enter. If a winner is discovered to be under 18, they will automatically be disqualified.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">The promotion is valid from 3/04/2018 to 30/04/2018</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Closing date for entry will be 30/04/2018 11:59pm AEST. After this date no further entries to the lucky draw will be accepted.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Promotion commences and closes on the dates shown. Only the winner(s) will be notified by email and must respond within 48 hours or the prize may be re-allocated. Relocation will be based on remaining competition entrants.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Winner(s) will be drawn on 2/05/2018.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Winner(s) name will may be published on our website and Facebook page.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">NCE reserves the right to share entry data with the prize promoter. On the occasion that this occurs, NCE will clearly specify this for privacy reasons.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">To enter, entrants must go in person to <object height="0"><a data-auto-recognition="true" href="http://www.nce.com.au/win" target="_blank">www.nce.com.au/win</a></object> during the open period and provide an answer to the question. Incomplete or automated entries will be disqualified. Multiple entries from the same IP address, same email address or from automated competition entry services are invalid.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">One entry per person and per household. Multiple entries from the same person or same household will be disqualified.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">There is no entry fee to enter the Supershow Giveaway. No responsibility can be accepted for entries not received for whatever reason.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">The promoters decision is final and no correspondence will be entered into.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Prize is not transferable or exchangeable and cannot be taken as cash. No responsibility is accepted for any variation in the value of the prize. Transport to and from an event and all other ancillary costs are the responsibility of the winner.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">All competition entrants must reside in the NCE Country (Australia) that the competition is housed within. Winners entering from outside the state will be disqualified.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">The Promoter reserves the right to, at any time, verify an entry or entrant and disqualify an entrant the Promoter has reason to believe has submitted an entry not in accordance with these Terms and Conditions.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">All entries in the competition become the property of the Promoter. The Promoter collects personal information from entrants to conduct the competition and may, in the course of business, disclose the personal information to third parties, as required. Entry in this competition is conditional on provision of the personal information requested. Entries may be entered into a database and the Promoter may use the entrants&#39; names and addresses for future promotional, marketing and publicity in various forms by the Promoter, and the entrant consents to such use. Entrants may direct any request to access their personal information to the Promoter. Additionally, all literary works submitted as part of an entry become the property of the Promoter and it is a condition of entry that those literary works may be used by the Promoter for their own promotional, marketing and publicity purposes without restriction. The promoter may share the entry details with the prize provider.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">The Promoter reserves the right to modify, suspend or terminate the competition without notice.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">The Promoter is not liable for any loss (including loss of opportunity) or damage (including, but not limited to, direct, indirect or inconsequential loss) or personal injury in relation to this competition or the use of, or participation in, the prize.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Terms and conditions of any prize tickets must be adhered to. Some events have age restrictions and these must also be adhered to.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">Prizes can only be sent to addresses in Australia and any tickets are only valid in Australia.</p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">This promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook or any other social network. You are providing your information to NCE Pty Ltd and not to any other party. The information provided will be used in conjunction with the following Privacy Policy found at: <span style="text-decoration:underline"><a href="https://www.nce.com.au/policies-privacy-returns" target="undefined">https://www.nce.com.au/policies-privacy-returns</a></span></p>
                                                    </li>
                                                    <li>
                                                    <p class="font_8" style="font-size:18px">These Terms &amp; Conditions will bind this and any future entry by you into a competition on this site and may be updated from time to time.</p>
                                                    </li>
                                                </ol>






                                        </div>
                                    </div>
                                     <!-- Tab panes end -->
                                </div>
                        </div>
                  </div>
              <!--<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>-->
            </div>
      </div>
</div>
</body>
</html>
